package com.example.demo;

import com.example.demo.Entity.Organization;
import com.example.demo.Entity.User;
import com.example.demo.Repository.OrganizationDao;
import com.example.demo.Repository.PaperDao;
import com.example.demo.Repository.PaperDao;
import com.example.demo.Service.OrganizationService;
import com.example.demo.Service.OrganizationService;
import com.example.demo.Service.PaperService;
import com.example.demo.Service.UserService;
import com.example.demo.Service.AuthorService;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import javax.annotation.Resource;
import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    PaperDao paperDao;
    @Autowired
    UserService userService;
    @Autowired
    OrganizationService organizationService;
    @Resource
    RestHighLevelClient client;
    @Autowired
    AuthorService authorService;
    @Autowired
    PaperService paperService;
    @Autowired
    OrganizationDao organizationDao;
    String paperDb="origin_papers";
//    @Test
//    void contextLoads() {
//    }
//
////    @Test
////    void operationTest(){
////        Paper paper=new Paper();
////        paper.setType("book");
////        paper.setISBN("ababab");
////        paperDao.save(paper);
////        System.out.println(paperDao.findPaperByISBN("ababab").getId());
////    }
//
//    @Test
//    void apiTest() throws IOException {
//        SearchRequest request=new SearchRequest();
//        request.indices(paperDb);
//        request.source(new SearchSourceBuilder().query(QueryBuilders.termQuery("id","53e998e8b7602d9702125d28")));
//        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
//        SearchHits searchHits=response.getHits();
//        for (SearchHit hit:searchHits){
//            System.out.println(hit.getSourceAsMap());
//        }
//        System.out.println("finish");
//    }
//
//    @Test
//    /*
//    对象数据查询
//          "venue" : {
//            "raw" : "Deutsche Medizinische Wochenschrift",
//            "id" : "5451a5c4e0cf0b02b5f38592"
//          }
//     */
//    void apiTest2() throws IOException {
//        String body="{\n" +
//                "    \"match_phrase\": {\n" +
//                "      \"venue.raw\":\"Linear Algebra and Its Applications\"\n" +
//                "    }\n" +
//                "  }";
//        //System.out.println(body);
//        SearchRequest request=new SearchRequest();
//        request.indices(paperDb);
//        request.source(new SearchSourceBuilder().query(QueryBuilders.wrapperQuery(body)));
//        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
//        SearchHits searchHits=response.getHits();
//        System.out.println(searchHits.getTotalHits().value);
//        for (SearchHit hit:searchHits){
//            System.out.println(hit.getSourceAsMap());
//        }
//        System.out.println("finish");
//    }
//
//    @Test
//    /*
//    普通数组数据查询，和普通的键值对形式的查询方式别无二致
//          "url" : [
//            "http://dx.doi.org/10.1055/s-2008-1070151",
//            "xxxx"
//          ]
//     */
//    void apiTest3() throws IOException {
//        String body="{\n" +
//                "    \"match_phrase\": {\n" +
//                "      \"url\": \"http://dx.doi.org/10.1016/0024-3795(86)90262-4\"\n" +
//                "    }\n" +
//                "  }";
//        //System.out.println(body);
//        SearchRequest request=new SearchRequest();
//        request.indices(paperDb);
//        request.source(new SearchSourceBuilder().query(QueryBuilders.wrapperQuery(body)));
//        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
//        SearchHits searchHits=response.getHits();
//        for (SearchHit hit:searchHits){
//            System.out.println(hit.getSourceAsMap());
//        }
//        System.out.println("finish");
//    }
//
//    @Test
//    /*
//    对象数组数据查询，和对象数据查询无区别
//          "authors" : [
//            {
//              "name" : "N. Soehendra",
//              "id" : "543243fadabfaeb4c6a7bfb5"
//            },
//            {
//              "name" : "I. Kempeneers",
//              "id" : "53f452fadabfaeb22f4f3738"
//            },
//            {
//              "name" : "K. de Heer",
//              "id" : "53f45faedabfaefedbb6ff36"
//            }
//          ]
//     */
//    void apiTest4() throws IOException {
//        String body="{\n" +
//                "    \"match_phrase\": {\n" +
//                "      \"authors.name\":\"N. Soehendra\"\n" +
//                "    }\n" +
//                "  }";
//        //System.out.println(body);
//        SearchRequest request=new SearchRequest();
//        request.indices(paperDb);
//        request.source(new SearchSourceBuilder().query(QueryBuilders.wrapperQuery(body)));
//        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
//        SearchHits searchHits=response.getHits();
//        for (SearchHit hit:searchHits){
//            System.out.println(hit.getSourceAsMap());
//        }
//        System.out.println("finish");
//    }
//
//    @Test
//    void test() throws IOException {
//        String authorId1="53f47b8bdabfaeb22f56e1bd";
//        String authorId2="53f432eddabfaee02aca62e9";
//        BoolQueryBuilder boolQueryBuilder=new BoolQueryBuilder();
//        boolQueryBuilder.should(paperService.authorIdQB(authorId1));
//        boolQueryBuilder.should(paperService.authorIdQB(authorId2));
//        List<Map<String, Object>> mapList = paperService.searchQuery(boolQueryBuilder, 0);
//        mapList.forEach(System.out::println);
//    }
//
//    @Test
//    void orgTest(){
//        String str="53f43f5adabfaedf435b9bdf";
//        System.out.println(str.length());
//    }
//    @Test
//    void script() throws IOException {
//        String body="{\n" +
//                "    \"match_all\": {}\n" +
//                "  }";
//        QueryBuilder queryBuilder=QueryBuilders.wrapperQuery(body);
//        Integer page=-1;
//        Integer count=0;
//        while (true){
//            page++;
//            List<Map<String, Object>> mapList = paperService.searchQuery(queryBuilder, page);
//            for(Map<String,Object> map:mapList){
//                List<Map<String,Object>> authorList= ((List<Map<String, Object>>) map.get("authors"));
//                for (Map<String,Object> author:authorList){
//                    if(authorService.getAuthorByAuthorId(author.get("id").toString())==null){
//                    }
//                    else {
//                        System.out.println(author.get("id").toString());
//                        System.out.println("success");
//                        count++;
//                        if(count==10){
//                            return;
//                        }
//                    }
//                }
//            }
//        }
//    }
    @Test
    public void orgTest(){
        organizationService.getHotOrganizations().forEach(System.out::println);
    }

}

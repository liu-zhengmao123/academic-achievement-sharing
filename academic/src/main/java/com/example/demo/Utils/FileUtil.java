package com.example.demo.Utils;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/** 上传文件工具类 **/
public class FileUtil {
    public static void uploadFile(InputStream inputStream, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);

        if(!targetFile.exists()){
            targetFile.mkdir();
        }
        FileOutputStream out = new FileOutputStream(filePath+"/"+fileName);
        IOUtils.copy(inputStream,out);
        out.flush();
        out.close();
        System.out.println("upload file success");
    }
}
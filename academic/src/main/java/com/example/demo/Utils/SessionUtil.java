package com.example.demo.Utils;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

/** session工具类，防止浏览器禁用cookies导致的错误 **/
public class SessionUtil {
    private static HashMap<String, HttpSession> mymap = new HashMap<>();

    public static synchronized void AddSession(HttpSession session) {
        if (session != null) {
            mymap.put(session.getId(), session);
        }
    }

    public static synchronized void DelSession(HttpSession session) {
        if (session != null) {
            mymap.remove(session.getId());
        }
    }

    public static synchronized HttpSession getSession(String session_id) {
        if (session_id == null)
            return null;
        HttpSession session = mymap.get(session_id);
        if(session==null){
            return null;
        }
//        if (session.getAttribute("userId")==null){
//            DelSession(session);
//            return null;
//        }
        return session;
    }
}





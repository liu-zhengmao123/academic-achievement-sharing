package com.example.demo.Utils;

/** 自定义的异常,继承RuntimeException以保证事务回滚 **/
public class OperationFailed extends RuntimeException{
    public OperationFailed(String msg) {
        super(msg);
    }
}

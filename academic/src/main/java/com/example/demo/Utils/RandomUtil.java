package com.example.demo.Utils;

import java.util.List;
import java.util.Random;

public class RandomUtil {
    public static String generateId(){
        final Integer length=24;
        final String str="0123456789abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<length;i++){
            sb.append(str.charAt(new Random().nextInt(36)));
        }
        return sb.toString();
    }
}

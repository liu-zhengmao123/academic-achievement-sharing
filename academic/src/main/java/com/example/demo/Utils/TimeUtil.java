package com.example.demo.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//所有日期有关的数据统一使用yyyy-MM-dd HH:mm:ss格式
public class TimeUtil {
    static final SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static Date strToDate(String str){
        try {
            Date date=sdf.parse(str);
            return date;
        }
        catch (Exception e){
            throw new OperationFailed("时间格式不正确");
        }
    }

    public static String dateToStr(Date date){
        return sdf.format(date);
    }

    public static Calendar dateToCal(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

}

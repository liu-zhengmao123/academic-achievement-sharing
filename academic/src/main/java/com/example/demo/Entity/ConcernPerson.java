package com.example.demo.Entity;

import javax.persistence.*;

@Entity
public class ConcernPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
    @OneToOne
    private User concerned;
    private String concernedName;
    private String concernedPic;

    public ConcernPerson(){
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getConcerned() {
        return concerned;
    }

    public void setConcerned(User concernedPerson) {
        this.concerned = concernedPerson;
    }

    public String getConcernedName() {
        return concernedName;
    }

    public void setConcernedName(String concernedName) {
        this.concernedName = concernedName;
    }

    public String getConcernedPic() {
        return concernedPic;
    }

    public void setConcernedPic(String concernedPic) {
        this.concernedPic = concernedPic;
    }
}

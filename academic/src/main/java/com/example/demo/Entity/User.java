package com.example.demo.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;
    private String userName;
    private String email;
    private String phone;
    private String password;
    private int userType;//0为管理员，1为普通用户，2为学者
    @ManyToOne(targetEntity = Organization.class)
    @JoinColumn(name = "organization_id", referencedColumnName = "organization_id")
    private Organization organization;
    private boolean apply;//true,正在申请;false,已经加入或者没有组织
    private Date registerTime;

    private String description;
    private boolean sex;//true为男，false为女
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String education;
    private boolean isBan;//是否被封禁
    private Timestamp banTime;
    private String banReason;

    private String authorId;//对应es库中的作者id


    private String headPic;
    private String specialty="";
    @JsonIgnore
    private String IdPic;//身份证照片,序列化时忽略保证隐私

    private int temperature;//点击数

    @OneToMany(mappedBy = "user")
    private List<History> history=new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Comment> commentList=new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Folder> folderList=new ArrayList<>();

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer userId) {
        this.id = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public boolean isBan() {
        return isBan;
    }

    public void setBan(boolean ban) {
        isBan = ban;
    }

    public Timestamp getBanTime() {
        return banTime;
    }

    public void setBanTime(Timestamp banTime) {
        this.banTime = banTime;
    }

    public String getBanReason() {
        return banReason;
    }

    public void setBanReason(String banReason) {
        this.banReason = banReason;
    }

    public List<Folder> getFolderList() {
        return folderList;
    }

    public void setFolderList(List<Folder> folderList) {
        this.folderList = folderList;
    }

    public String getIdPic() {
        return IdPic;
    }

    public void setIdPic(String idPic) {
        IdPic = idPic;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public boolean isApply() {
        return apply;
    }

    public void setApply(boolean apply) {
        this.apply = apply;
    }
}

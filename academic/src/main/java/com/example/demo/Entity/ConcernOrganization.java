package com.example.demo.Entity;


import javax.persistence.*;

@Entity
public class ConcernOrganization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
    @OneToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;
    private String organizationName;
    private String organizationPic;

    public ConcernOrganization(){
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationPic() {
        return organizationPic;
    }

    public void setOrganizationPic(String organizationPic) {
        this.organizationPic = organizationPic;
    }
}


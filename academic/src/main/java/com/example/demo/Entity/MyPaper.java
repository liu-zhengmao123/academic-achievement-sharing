package com.example.demo.Entity;

import javax.persistence.*;

@Entity
public class MyPaper {//这个表是否需要？
    @Id
    @GeneratedValue
    private Integer id;
    private Integer paperId;
    private Integer paperTitle;
    private int temperature;//点击数
    public MyPaper(){
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPaperId() {
        return paperId;
    }

    public void setPaperId(Integer paperId) {
        this.paperId = paperId;
    }

    public Integer getPaperTitle() {
        return paperTitle;
    }

    public void setPaperTitle(Integer paperTitle) {
        this.paperTitle = paperTitle;
    }

}

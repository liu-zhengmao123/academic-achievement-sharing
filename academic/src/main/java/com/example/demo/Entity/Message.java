package com.example.demo.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String content;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;
    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "from_user_id",referencedColumnName = "user_id")
    private User fromUser;

    @ManyToOne(targetEntity = Organization.class)
    @JoinColumn(name = "from_organization_id",referencedColumnName = "organization_id")
    private Organization fromOrganization;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "to_user_id",referencedColumnName = "user_id")
    private User toUser;

    private int type; //1:user to user; 2:organization to user; 3:system to user

    private boolean hasRead =false;

    public boolean isHasRead() {
        return hasRead;
    }

    public void setHasRead(boolean read) {
        this.hasRead = read;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User from_user) {
        this.fromUser = from_user;
    }

    public Organization getFromOrganization() {
        return fromOrganization;
    }

    public void setFromOrganization(Organization from_organization) {
        this.fromOrganization = from_organization;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User to_user) {
        this.toUser = to_user;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

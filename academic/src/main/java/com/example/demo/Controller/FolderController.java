package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.Favorite;
import com.example.demo.Entity.Folder;
import com.example.demo.Entity.User;
import com.example.demo.Service.FavoriteService;
import com.example.demo.Service.FolderService;
import com.example.demo.Service.PaperService;
import com.example.demo.Service.UserService;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FolderController {
    @Autowired
    FolderService folderService;
    @Autowired
    UserService userService;
    @Autowired
    PaperService paperService;
    @Autowired
    FavoriteService favoriteService;
    @GetMapping("/get_myfavorites")
    JSONObject getAllFolders(@RequestHeader("ssid")String ssid){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        List<Folder> folderList=folderService.getFoldersByUser(user);
        JSONArray array = new JSONArray();
        for(Folder folder:folderList){
            HashMap<String,Object> info=new HashMap<>();
            info.put("name",folder.getName());
            info.put("id",folder.getId());
            info.put("description",folder.getDescription());
            List<Map<String,Object>> papers=new ArrayList<>();
            for(Favorite paper:folder.getPaperList()){
                HashMap<String,Object> paperInfo=new HashMap<>();
                paperInfo.put("paper_id",paper.getPaperId());
                paperInfo.put("title",paper.getPaperTitle());
                papers.add(paperInfo);
            }
            info.put("papers",papers);
            array.add(info);
        }
        ret.put("success",true);
        ret.put("exc","");
        ret.put("folders",array);
        return ret;
    }

    @PostMapping("/add_folder")
    JSONObject addFolder(@RequestHeader("ssid")String ssid,@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        Folder folder=new Folder();
        folder.setName(params.get("name").toString());
        folder.setDescription("");
        folder.setUser(user);
        folderService.save(folder);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/cancel/favorite")
    JSONObject cancelFavorite(@RequestHeader("ssid")String ssid,@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        String paperId=params.get("paper_id").toString();
        List<Folder> folderList=folderService.getFoldersByUser(user);
        Favorite delete=null;
        for(Folder folder:folderList){
            for(Favorite favorite:folder.getPaperList()){
                if(favorite.getPaperId().equals(paperId)){
                    delete=favorite;
                }
            }
            if(delete!=null)folder.getPaperList().remove(delete);
            folderService.save(folder);
        }
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/add_favorite")
    JSONObject addFavorite(@RequestHeader("ssid")String ssid,@RequestBody JSONObject params) throws IOException {
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        String paperId=params.get("paper_id").toString();
        int folderId=Integer.parseInt(params.get("folder_id").toString());
        Map<String,Object> paper=paperService.getPaperByPaperId(paperId);
        if(paper==null)throw new OperationFailed("没有找到文章");
        Favorite favorite=new Favorite();
        favorite.setPaperId(paperId);
        favorite.setPaperTitle(paper.get("title").toString());
        favoriteService.save(favorite);
        Folder folder=folderService.getById(folderId);
        if(folder==null)throw new OperationFailed("未找到收藏夹");
        List<Favorite> favorites=folder.getPaperList();
        favorites.add(favorite);
        folder.setPaperList(favorites);
        folderService.save(folder);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/favorite/edit_file_name")
    JSONObject editFolderName(@RequestHeader("ssid")String ssid,@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        int folderId=Integer.parseInt(params.get("file_id").toString());
        String folderName=params.get("file_name").toString();
        List<Folder> folderList=user.getFolderList();
        for(Folder folder:folderList){
            if(folder.getId()==folderId){
                folder.setName(folderName);
                folderService.save(folder);
            }
        }
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/favorite/edit_file_description")
    JSONObject editFolderDes(@RequestHeader("ssid")String ssid,@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        int folderId=Integer.parseInt(params.get("file_id").toString());
        String folderDes=params.get("file_description").toString();
        List<Folder> folderList=user.getFolderList();
        for(Folder folder:folderList){
            if(folder.getId()==folderId){
                folder.setDescription(folderDes);
                folderService.save(folder);
            }
        }
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/favorite/delete")
    JSONObject deleteFolder(@RequestHeader("ssid")String ssid,@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        if(user==null)throw new OperationFailed("用户不存在");
        int folderId=Integer.parseInt(params.get("id").toString());
        List<Folder> folderList=user.getFolderList();
        folderList.removeIf(folder -> folder.getId() == folderId);
        user.setFolderList(folderList);
        userService.save(user);
        Folder folder=folderService.getById(folderId);
        folderService.delete(folder);
        ret.put("success",true);
        return ret;
    }
}

package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.*;
import com.example.demo.Service.EmailService;
import com.example.demo.Service.ReportService;
import com.example.demo.Service.UserService;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.SessionUtil;
import com.example.demo.Utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    EmailService emailService;
    @Autowired
    ReportService reportService;
    @PostMapping("/register/get_code")
    JSONObject getVerificationCode(HttpServletRequest request,
                                   @RequestBody JSONObject params) throws MessagingException {
        JSONObject ret = new JSONObject();
        String email = params.get("email").toString();
        if(userService.getUserByEmail(email)!=null)throw new OperationFailed("邮箱已注册");
        Random random = new Random();
        int code = 1000 + random.nextInt(9000); // 四位数字验证
        //发送邮件
        emailService.sendEmail(email,"【Scholar平台注册验证】","欢迎您注册Scholar学术平台，您的验证码是"+code);
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("code",code);
        SessionUtil.AddSession(httpSession);
        ret.put("ssid",httpSession.getId());
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }
    @PostMapping("/register")
    JSONObject register(@RequestHeader("ssid")String ssid,
                        @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        String email = params.get("email").toString();
        if(userService.getUserByEmail(email)!=null)throw new OperationFailed("邮箱已注册");
        HttpSession httpSession = SessionUtil.getSession(ssid);
        Object code = httpSession.getAttribute("code");
        if(code==null)throw new OperationFailed("请重新获取验证码");
        int userCode = params.getInteger("verification_code");
        if((Integer)code!=userCode)throw new OperationFailed("验证码错误");
        // 验证成功了
        User user = new User();
        user.setEmail(email);
        user.setUserName(params.getString("user_name"));
        user.setPassword(params.getString("password"));
        user.setHeadPic(params.getString("head_pic"));
        user.setRegisterTime(new Date());
        user.setUserType(1);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }
    @PostMapping("/login")
    JSONObject login(HttpServletRequest request,
                              @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        String email = params.getString("username");
        String password = params.getString("password");
        User user = userService.getUserByEmail(email);
        if(user==null)throw new OperationFailed("用户不存在");
        if(!user.getPassword().equals(password))throw new OperationFailed("密码不匹配");
        if(user.isBan())throw new OperationFailed("用户没有登录权限");
        HttpSession session = request.getSession();
        SessionUtil.AddSession(session);
        session.setAttribute("userId",user.getId());
        if(user.getUserType()==0){
            //session.setAttribute("admin",user.getUserName());
            ret.put("type","admin");
        }
        else ret.put("type","user");
        ret.put("user_id",user.getId());
        ret.put("success",true);
        ret.put("ssid",session.getId());
        ret.put("exc","");
        return ret;
    }
    @GetMapping("/user/black_list")
    public JSONObject getBlackList(){
        JSONObject ret = new JSONObject();
        JSONArray userList = new JSONArray();
        List<User> banUsers = userService.getAllByBan(true);
        userList.addAll(banUsers);
        ret.put("success",true);
        ret.put("exc","");
        ret.put("list",userList);
        return ret;
    }

    @PostMapping("/user/not_ban")
    public JSONObject unbanAUser(@RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        User user = userService.getUserById(params.getInteger("userId"));
        user.setBan(false);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/info/add_specialty")
    public JSONObject addSpecialty(@RequestHeader("ssid")String ssid,
                                   @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null)throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        String specialty = params.get("specialty").toString();
        String user_specialty = user.getSpecialty();
        if(user_specialty==null) user_specialty= "";
        List<String> list = user_specialty.isEmpty() ?
                new ArrayList<>():
                new ArrayList<>(Arrays.asList(user_specialty.split(",")));
        if(list.contains(specialty)){
            ret.put("success",2);
            ret.put("exc","该领域已存在");
        }else{
            list.add(specialty);
            String ret_list = String.join(",", list);
            user.setSpecialty(ret_list);
            userService.save(user);
            ret.put("success",1);
            ret.put("exc","");
        }
        return ret;
    }

    @PostMapping("/info/remove_specialty")
    public JSONObject removeSpecialty(@RequestHeader("ssid")String ssid,
                                   @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) {
            ret.put("success",false);
            ret.put("exc","用户未登录");
            return ret;
        }
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        String specialty = params.get("specialty").toString();
        String user_specialty = user.getSpecialty();
        if(user_specialty==null) user_specialty= "";
        List<String> list = user_specialty.isEmpty() ?
                new ArrayList<>():
                new ArrayList<>(Arrays.asList(user_specialty.split(",")));
        list.remove(specialty);
        String ret_list = String.join(",", list);
        user.setSpecialty(ret_list);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","");

        return ret;
    }

    @PostMapping("/info/edit_birthday")
    public JSONObject editBirthday(@RequestHeader("ssid")String ssid,
                                   @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            user.setBirthday(sdf.parse(params.get("birthday").toString()));
            userService.save(user);
            ret.put("success",true);
            ret.put("exc","");
        }catch (Exception e){
            ret.put("success",false);
            ret.put("exc",e.getMessage());
        }
        return ret;
    }

    @PostMapping("/info/edit_description")
    public JSONObject editDescription(@RequestHeader("ssid")String ssid,
                                   @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        user.setDescription(params.get("description").toString());
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","");

        return ret;
    }

    @PostMapping("/info/edit_sex")
    public JSONObject editSex(@RequestHeader("ssid")String ssid,
                                      @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        user.setSex(params.get("sex").toString().equals("男"));
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","");

        return ret;
    }

    @PostMapping("/info/edit_username")
    public JSONObject editUsername(@RequestHeader("ssid")String ssid,
                              @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        user.setUserName(params.get("username").toString());
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","");

        return ret;
    }

    @GetMapping("/info/get_info")
    public JSONObject getInfo(@RequestHeader("ssid")String ssid){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        ret.put("phone",user.getPhone());
        ret.put("email",user.getEmail());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(user.getBirthday()!=null)ret.put("birthday",sdf.format(user.getBirthday()));
        else ret.put("birthday","");
        ret.put("description",user.getDescription());
        ret.put("sex",user.isSex()?"男":"女");
        ret.put("user_id",user.getId());
        ret.put("username",user.getUserName());
        ret.put("specialty",user.getSpecialty());

        return ret;
    }

    @GetMapping("/user/info")
    public JSONObject getSomeBodyInfo(@RequestParam("user_id")int user_id){
        JSONObject ret = new JSONObject();
        User user = userService.getUserById(user_id);
        if(user==null) throw new OperationFailed("未找到用户");

        ret.put("phone",user.getPhone());
        ret.put("email",user.getEmail());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(user.getBirthday()!=null)ret.put("birthday",sdf.format(user.getBirthday()));
        else ret.put("birthday","");
        ret.put("description",user.getDescription());
        ret.put("sex",user.isSex()?"男":"女");
        ret.put("username",user.getUserName());
        ret.put("specialty",user.getSpecialty());

        return ret;
    }

    @GetMapping("/info/get_my_organization")
    public JSONObject getOrganization(@RequestHeader("ssid")String ssid){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        Organization organization=user.getOrganization();

        ret.put("organization_id",organization!=null?organization.getId():"");
        ret.put("organization_name",organization!=null?organization.getOrganizationName():"");
        ret.put("is_manager",organization!=null&&organization.getAdmin().getId().equals(user.getId()));
        ret.put("header_pic",organization!=null?organization.getHead_pic():"");
        return ret;
    }

    @GetMapping("/history/get_all")
    public JSONObject getHistory(@RequestHeader("ssid")String ssid){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        List<History> histories = user.getHistory();
        JSONArray history_records=new JSONArray();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (History i :
                histories) {
            JSONObject history=new JSONObject();
            history.put("id",i.getId());
            history.put("time",sdf.format(i.getTime()));
            history.put("title",i.getPaperTitle());
            history.put("paper_id",i.getPaperId());
            history_records.add(history);
        }

        ret.put("history_records",history_records);

        return ret;
    }

    @PostMapping("/history/delete")
    public JSONObject deleteHistory(@RequestHeader("ssid")String ssid,
                                    @RequestBody JSONObject params){
        JSONObject ret = new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);

        String history_id=params.get("history_id").toString();
        JSONArray histories=JSONArray.parseArray(history_id);
        for (Object i :
                histories) {
            try{
                History history = userService.findHistoryById((Integer) i);
                if (history != null) userService.deleteHistoryById((Integer) i);
                else throw new Exception("历史记录id不存在");
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        ret.put("success",true);

        return ret;
    }

    @PostMapping("/ban/user")
    public JSONObject banUser(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        try {
            Integer userId = params.getInteger("user_id");
            User user = userService.getUserById(userId);
            user.setBan(true);
            userService.save(user);
        }
        catch (Exception e){
            throw new OperationFailed("这接口也能错?");
        }
        ret.put("success",true);
        ret.put("exc","封禁成功");
        return ret;
    }

    @GetMapping("/list/user")
    public JSONObject getUserList(){
        JSONObject ret=new JSONObject();
        List<User> userList = userService.getAllUsers();
        JSONArray array = new JSONArray();
        for(User user:userList){
            JSONObject info = new JSONObject();
            info.put("user_name",user.getUserName());
            info.put("email",user.getEmail());
            Date date = user.getRegisterTime();
            if(date==null)info.put("time","");
            else info.put("time", TimeUtil.dateToStr(date));
            Organization organization = user.getOrganization();
            if(organization==null)info.put("organization","");
            else info.put("organization",organization.getOrganizationName());
            array.add(info);
        }
        ret.put("list",array);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/door/get_door_judge")
    public JSONObject getPortalJudge(@RequestHeader("ssid")String ssid){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null||session.getAttribute("userId")==null)throw new OperationFailed("请重新登录");
        User user = userService.getUserById((Integer) session.getAttribute("userId"));
        if(user.getAuthorId()==null||user.getAuthorId().equals(""))ret.put("has_door",false);
        else ret.put("has_door",true);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/info/get_header_pic")
    public JSONObject getHeaderPic(@RequestHeader("ssid")String ssid){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null||session.getAttribute("userId")==null)throw new OperationFailed("请重新登录");
        User user = userService.getUserById((Integer) session.getAttribute("userId"));
        if(user.getHeadPic()==null)ret.put("header_pic","");
        else ret.put("header_pic",user.getHeadPic());
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/user/get_author_id")
    public JSONObject getAuthorId(@RequestParam("user_id")Integer userId)
    {
        JSONObject ret=new JSONObject();
        User user = userService.getUserById(userId);
        ret.put("author_id",user.getAuthorId()!=null?user.getAuthorId():"");
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }
}

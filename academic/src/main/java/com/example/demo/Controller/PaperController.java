package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.*;
import com.example.demo.Service.*;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.RandomUtil;
import com.example.demo.Utils.SessionUtil;
import com.example.demo.Utils.TimeUtil;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class PaperController {
    @Autowired
    UserService userService;
    @Autowired
    PaperService paperService;
    @Autowired
    CommentService commentService;
    @Autowired
    AuthorService authorService;
    @Autowired
    FolderService folderService;
    @Autowired
    OrganizationService organizationService;
    @Autowired
    HistoryService historyService;
    @PostMapping("/author/get_all_paper")
    JSONObject getAllPaperByAuthor(@RequestBody JSONObject params, @RequestHeader("ssid") String ssid){
        String authorId=params.getString("author_id");
        Integer page=params.getInteger("page");
        JSONObject ret=new JSONObject();
        boolean islogin=false;
        User user=null;
        if(ssid!=null) {
            HttpSession session = SessionUtil.getSession(ssid);
            if (session == null) islogin=false;
            else {
                int user_id = (int) session.getAttribute("userId");
                user = userService.getUserById(user_id);
                islogin = true;
            }
        }
        List<Map<String,Object>> paperMapList=null;
        Integer totalPages=null;
        try {
            QueryBuilder QB=paperService.authorIdQB(authorId);
            paperMapList = paperService.searchQuery(QB,page);
            totalPages = paperService.getTotalPages(QB);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray papers=new JSONArray();
        try {
            for (Map<String, Object> paperMap : paperMapList) {
                JSONObject paper = new JSONObject();
                paper.put("download_num", paperMap.get("download_num")!=null?paperMap.get("download_num"):0);//TODO:下载量
                paper.put("cited", paperMap.get("n_citation"));
                Map<String, Object> venue = (Map<String, Object>) paperMap.get("venue");
                try {
                    paper.put("database", venue.get("raw"));
                }
                catch (Exception e){
                    paper.put("database","");
                }
                paper.put("time", paperMap.get("year").toString() + "年");
                paper.put("title", paperMap.get("title"));
                paper.put("author_name", paperService.getAllAuthorNamesOfPaper(paperMap));
                paper.put("paper_id", paperMap.get("id"));
                paper.put("is_store", islogin&&folderService.isStored(user,paperMap.get("id").toString()));
                papers.add(paper);
            }
        }
        catch (Exception e){
            throw new OperationFailed("文献信息异常");
        }
        ret.put("success",true);
        ret.put("exc","success");
        ret.put("papers",papers);
        ret.put("total_pages",totalPages);
        return ret;
    }

    @GetMapping("/paper/get_comment")
    JSONObject getComment(@RequestParam("paper_id") String paperId){
        JSONObject ret=new JSONObject();
        List<Comment> paper_comment=commentService.getCommentByPaperId(paperId);
        JSONArray comments=new JSONArray();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            for (Comment comment_ : paper_comment) {
                JSONObject comment = new JSONObject();
                comment.put("comment_id", comment_.getId());
                comment.put("user_name", comment_.getUser().getUserName());
                comment.put("user_id", comment_.getUser().getId());
                comment.put("header_pic", comment_.getUser().getHeadPic());
                comment.put("content", comment_.getContent());
                comment.put("time", sdf.format(comment_.getTime()));
                comment.put("is_top", comment_.isTop());
                comments.add(comment);
            }
        }
        catch (Exception e){
            throw new OperationFailed("评论信息异常");
        }
        ret.put("success",true);
        ret.put("exc","success");
        ret.put("comments",comments);
        return ret;
    }

    @PostMapping("/paper/comment")
    JSONObject addComment(@RequestHeader("ssid") String ssid,
                          @RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        String paper_id=params.get("paper_id").toString();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null||session.getAttribute("userId")==null)throw new OperationFailed("请重新登录");
        User user = userService.getUserById((Integer) session.getAttribute("userId"));
        String time=params.get("time").toString();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String content=params.get("content").toString();

        try{
            Comment comment = new Comment();
            comment.setPaperId(paper_id);
            //User user=userService.getUserById(user_id);
            comment.setUser(user);
            comment.setTime(sdf.parse(time));
            comment.setContent(content);

            String author_id=user.getAuthorId();
            boolean is_author=false;
            if(author_id!=null&&!author_id.equals("")){
                Map<String,Object> paper=paperService.getPaperByPaperId(paper_id);
                String[] authors=paperService.getAllAuthorIdsOfPaper(paper).split(",");
                for (String s :
                        authors) {
                    if (s.equals(author_id)){
                        is_author=true;
                        break;
                    }
                }
            }
            comment.setAuthor(is_author);
            commentService.save(comment);
        }catch (Exception e){
            throw new OperationFailed("添加评论异常");
        }

        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }

    @PostMapping("/comment/top")
    JSONObject topAComment(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int commentId=params.getInteger("comment_id");
        Comment comment = commentService.getCommentById(commentId);
        comment.setTop(true);
        commentService.save(comment);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @PostMapping("/delete/comment")
    JSONObject deleteAComment(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int commentId=params.getInteger("comment_id");
        Comment comment = commentService.getCommentById(commentId);
        if(comment==null)throw new OperationFailed("评论已删除");
        commentService.delete(comment);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }

    final Map<Integer, String> keyMap=new HashMap<Integer, String>(){
        {
            put(1,"year");
            put(2,"n_citation");
            put(3,"download_num");
        }
    };
    final Map<Integer, SortOrder> orderMap=new HashMap<Integer, SortOrder>(){
        {
            put(1,SortOrder.DESC);
            put(2,SortOrder.ASC);
        }
    };
    final Map<Integer, String> typeMap=new HashMap<Integer, String>(){
        {
            put(1,"book");
            put(2,"periodical");
        }
    };
    @GetMapping("/search")
    JSONObject commonSearch(@RequestParam("search") String search, @RequestParam("page") Integer page,
                            @RequestParam("sortway") Integer sortway, @RequestParam("order") Integer order,
                            @RequestParam("paper_type") Integer paperType,@RequestHeader("ssid") String ssid){
        JSONObject ret=new JSONObject();
        BoolQueryBuilder commonQuery=new BoolQueryBuilder();
        BoolQueryBuilder shouldQuery=new BoolQueryBuilder();
        shouldQuery.should(paperService.abstractQB(search))
                .should(paperService.titleQB(search))
                .should(paperService.keyWordQB(search))
                .should(paperService.authorNameQB(search));
        commonQuery.must(shouldQuery);
        List<Map<String,Object>> paperMapList=null;
        boolean islogin=false;
        User user=null;
        if(ssid!=null) {
            HttpSession session = SessionUtil.getSession(ssid);
            if (session == null) islogin=false;
            else {
                int user_id = (int) session.getAttribute("userId");
                user = userService.getUserById(user_id);
                islogin = true;
            }
        }
        Integer totalPages=null;
        try {
            if(paperType!=0) commonQuery.must(paperService.typeQB(typeMap.get(paperType))).must(shouldQuery);
            if(sortway==0&&order==0) {
                paperMapList = paperService.searchQuery(commonQuery, page);
            }
            else {
                paperMapList = paperService.searchQuery(commonQuery,page,new HashMap<String, SortOrder>(){{put(keyMap.get(sortway),orderMap.get(order));}});
            }
            totalPages = paperService.getTotalPages(commonQuery);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray paperList=new JSONArray() ;
        for(Map<String, Object> paperMap:paperMapList){
            JSONObject paper=new JSONObject();
            paper.put("id",paperMap.get("id"));
            paper.put("name",paperMap.get("title"));
            paper.put("author",paperMap.get("authors"));
            Map<String, Object> venue = (Map<String, Object>) paperMap.get("venue");
            try {
                paper.put("source", venue.get("raw"));
            }
            catch (Exception e){
                paper.put("source","");
            }
            paper.put("quoted",paperMap.get("n_citation"));
            paper.put("date", paperMap.get("year").toString() + "年");
            paper.put("pdf",paperMap.get("pdf")!=null?paperMap.get("pdf"):"");
            try {
                List<String> urlList = ((List<String>) paperMap.get("url"));
                paper.put("url", urlList.size() != 0 ? urlList.get(0) : "");
            }
            catch (Exception e){
                paper.put("url",paperMap.get("url")!=null?paperMap.get("url").toString():"");
            }
            paper.put("download_num",paperMap.get("download_num")!=null?paperMap.get("download_num"):0);
            paper.put("is_store", islogin&&folderService.isStored(user,paperMap.get("id").toString()));
            paperList.add(paper);
        }
        ret.put("paperlist",paperList);
        ret.put("total_pages",totalPages);
        return ret;
    }

    @PostMapping("/advance_search")
    JSONObject advancedSearch(@RequestBody JSONObject params, @RequestHeader("ssid") String ssid){
        JSONObject ret=new JSONObject();
        BoolQueryBuilder advancedQuery= QueryBuilders.boolQuery();
        BoolQueryBuilder shouldQuery=QueryBuilders.boolQuery();
        String startTime = params.getString("start_time");
        String endTime = params.getString("end_time");
        Integer page = params.getInteger("page");
        Integer sortway = params.getInteger("sortway");
        Integer order = params.getInteger("order");
        Integer paperType = params.getInteger("paper_type");
        if(startTime!=null&&endTime!=null&&!startTime.equals("")&&!endTime.equals("")) {
            Date startDate = TimeUtil.strToDate(startTime);
            Date endDate = TimeUtil.strToDate(endTime);
            Calendar startCal = TimeUtil.dateToCal(startDate);
            Calendar endCal = TimeUtil.dateToCal(endDate);
            Integer startYear = startCal.get(Calendar.YEAR);
            Integer endYear = endCal.get(Calendar.YEAR);
            QueryBuilder QB= paperService.yearQB(startYear,endYear);
            advancedQuery.must(QB);
        }
        boolean islogin=false;
        User user=null;
        if(ssid!=null) {
            HttpSession session = SessionUtil.getSession(ssid);
            if (session == null) islogin=false;
            else {
                int user_id = (int) session.getAttribute("userId");
                user = userService.getUserById(user_id);
                islogin = true;
            }
        }
        JSONArray conditions=params.getJSONArray("conditions");
        for (int i=0;i<conditions.size();i++){
            JSONObject condition=conditions.getJSONObject(i);
            String keyWord=condition.getString("keywords");
            String type=condition.getString("type");
            String logic=condition.getString("logical_connectives");
            QueryBuilder QB=null;
            if(type.equals("主题")){
                new OperationFailed("按主题搜索功能敬请期待");
            }
            else if(type.equals("文献名")){
                QB=paperService.titleQB(keyWord);
            }
            else if(type.equals("作者")){
                QB=paperService.authorNameQB(keyWord);
            }
            else if(type.equals("关键词")){
                QB=paperService.keyWordQB(keyWord);
            }
            else if(type.equals("摘要")){
                QB=paperService.abstractQB(keyWord);
            }
            else if(type.equals("ISBN号")){
                QB=paperService.isbnQB(keyWord);
            }
            else if(type.equals("ISSN号")){
                QB=paperService.issnQB(keyWord);
            }
            else if(type.equals("DOI号")){
                QB=paperService.doiQB(keyWord);
            }
            else {
                throw new OperationFailed("错误的type取值");
            }
            if(logic.equals("AND")){
                advancedQuery.must(QB);
            }
            else if(logic.equals("OR")){
                shouldQuery.should(QB);
            }
            else if(logic.equals("NOT")){
                advancedQuery.mustNot(QB);
            }
            else {
                throw new OperationFailed("错误的逻辑连接词取值");
            }
        }
        advancedQuery.must(shouldQuery);
        List<Map<String,Object>> paperMapList=null;
        Integer totalPages=null;
        try {
            if(paperType!=0) advancedQuery.must(paperService.typeQB(typeMap.get(paperType)));
            if(sortway==0&&order==0) {
                paperMapList = paperService.searchQuery(advancedQuery, page);
            }
            else {
                paperMapList = paperService.searchQuery(advancedQuery,page,new HashMap<String, SortOrder>(){{put(keyMap.get(sortway),orderMap.get(order));}});
            }
            totalPages = paperService.getTotalPages(advancedQuery);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray data=new JSONArray();
        for(Map<String,Object> paperMap:paperMapList){
            JSONObject paper=new JSONObject();
            paper.put("name",paperMap.get("title"));
            paper.put("author",paperMap.get("authors"));
            paper.put("id",paperMap.get("id"));
            Map<String, Object> venue = (Map<String, Object>) paperMap.get("venue");
            try {
                paper.put("source", venue.get("raw"));
            }
            catch (Exception e){
                paper.put("source","");
            }
            paper.put("date", paperMap.get("year").toString() + "年");
            paper.put("type",paperMap.get("type")!=null?paperMap.get("type"):"");
            paper.put("quoted",paperMap.get("n_citation"));
            paper.put("download_num",paperMap.get("download_num")!=null?paperMap.get("download_num"):0);
            paper.put("pdf",paperMap.get("pdf")!=null?paperMap.get("pdf"):"");
            paper.put("is_store", islogin&&folderService.isStored(user,paperMap.get("id").toString()));
            try {
                List<String> urlList = ((List<String>) paperMap.get("url"));
                paper.put("url", urlList.size() != 0 ? urlList.get(0) : "");
            }
            catch (Exception e){
                paper.put("url",paperMap.get("url")!=null?paperMap.get("url").toString():"");
            }
            data.add(paper);
        }
        ret.put("total_pages",totalPages);
        ret.put("success",true);
        ret.put("exc","无异常");
        ret.put("data",data);
        return ret;
    }

    @GetMapping("/paper/info")
    JSONObject getPaperInfo(@RequestParam("paper_id") String paperId,@RequestHeader("ssid") String ssid){
        JSONObject ret=new JSONObject();
        Map<String,Object> paperMap=null;
        try {
            paperMap = paperService.getPaperByPaperId(paperId);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        boolean islogin=false;
        User user=null;
        if(ssid!=null) {
            HttpSession session = SessionUtil.getSession(ssid);
            if (session == null||session.getAttribute("userId")==null) islogin=false;
            else {
                int user_id = (int) session.getAttribute("userId");
                user = userService.getUserById(user_id);
                islogin = true;
            }
        }
        ret.put("name",paperMap.get("title"));
        ret.put("publish_time",paperMap.get("year").toString()+"年");
        Map<String, Object> venue = (Map<String, Object>) paperMap.get("venue");
        try {
            ret.put("source", venue.get("raw"));
        }
        catch (Exception e){
            ret.put("source","");
        }
        ret.put("cited",paperMap.get("n_citation"));
        ret.put("download_num",paperMap.get("download_num")!=null?paperMap.get("download_num"):0);
        ret.put("abstract", paperMap.get("abstract"));
        ret.put("pdf",paperMap.get("pdf")!=null?paperMap.get("pdf"):"");
        try {
            List<String> urlList = ((List<String>) paperMap.get("url"));
            ret.put("url", urlList.size() != 0 ? urlList.get(0) : "");
        }
        catch (Exception e){
            ret.put("url",paperMap.get("url")!=null?paperMap.get("url").toString():"");
        }
        ret.put("citation",paperService.exportCitation(paperMap));
        ret.put("is_stored",islogin&&folderService.isStored(user, paperMap.get("id").toString()));
        JSONArray authorList=new JSONArray();
        List<Map<String,Object>> authorMapList= (List<Map<String, Object>>) paperMap.get("authors");
        for(Map<String,Object> authorMap:authorMapList){
            JSONObject author=new JSONObject();
            author.put("name",authorMap.get("name")!=null?authorMap.get("name"):"");
            author.put("id",authorMap.get("id")!=null?authorMap.get("id"):"");
            if(authorMap.get("id")!=null){
                try {
                    Map<String, Object> author_ = authorService.getAuthorByAuthorId(authorMap.get("id").toString());
                    author.put("head_pic",author_.get("head_pic")!=null?author_.get("head_pic"):"");
                }
                catch (Exception e){
                    author.put("head_pic","");
                }
            }
            authorList.add(author);
        }
        if (islogin) historyService.save(user,paperMap);
        paperService.addClickTime(paperMap);
        ret.put("author_list",authorList);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/author/hot_paper")
    JSONObject getHotPapersByAuthor(@RequestParam("author_id") String authorId, @RequestHeader("ssid") String ssid){
        JSONObject ret=new JSONObject();
        boolean islogin=false;
        User user=null;
        if(ssid!=null) {
            HttpSession session = SessionUtil.getSession(ssid);
            if (session == null) islogin=false;
            else {
                int user_id = (int) session.getAttribute("userId");
                user = userService.getUserById(user_id);
                islogin = true;
            }
        }
        QueryBuilder QB = paperService.authorIdQB(authorId);
        List<Map<String, Object>> paperMapList=null;
        Map<String, SortOrder> sortBy=new HashMap<>();
        sortBy.put("click_time",SortOrder.DESC);
        try {
            paperMapList = paperService.searchQuery(QB, 0, sortBy);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        Integer length = paperMapList.size()<=5?paperMapList.size():5;
        JSONArray hotPapers=new JSONArray();
        for(int i=0;i<length;i++){
            Map<String,Object> paperMap=paperMapList.get(i);
            JSONObject hotPaper=new JSONObject();
            hotPaper.put("id",paperMap.get("id"));
            hotPaper.put("title",paperMap.get("title"));
            hotPaper.put("is_store",islogin&&folderService.isStored(user, paperMap.get("id").toString()));
            hotPaper.put("download_num",paperMap.get("download_num")!=null?paperMap.get("download_num"):0);
            hotPapers.add(hotPaper);
        }
        ret.put("success",true);
        ret.put("exc","");
        ret.put("hot_papers",hotPapers);
        return ret;
    }

    @GetMapping("/organization/get_paper")
    JSONObject getOrganizationPapers(@RequestParam("organization_id") Integer organizationId, @RequestParam("page") Integer page){
        JSONObject ret=new JSONObject();
        Organization organization=organizationService.getOrganizationById(organizationId);
        List<User> userList = organization.getUserList();
        BoolQueryBuilder QB=new BoolQueryBuilder();
        for(User user:userList){
            if(user.getAuthorId()!=null&&!user.getAuthorId().equals(""))
                QB.should(paperService.authorIdQB(user.getAuthorId()));
        }
        List<Map<String,Object>> paperMapList=null;
        Integer totalPages=null;
        try {
            paperMapList = paperService.searchQuery(QB,page);
            totalPages = paperService.getTotalPages(QB);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray papers=new JSONArray();
        for(Map<String,Object> paperMap:paperMapList){
            JSONObject paper=new JSONObject();
            paper.put("title",paperMap.get("title"));
            paper.put("database",paperMap.get("type")!=null?paperMap.get("type"):"");
            Map<String, Object> venue = (Map<String, Object>) paperMap.get("venue");
            try {
                paper.put("source", venue.get("raw"));
            }
            catch (Exception e){
                paper.put("source","");
            }
            paper.put("quote",paperMap.get("n_citation"));
            paper.put("paper_id",paperMap.get("id"));
            paper.put("time",paperMap.get("year").toString()+"年");
            paper.put("download",paperMap.get("download_num")!=null?paperMap.get("download_num"):0);
            papers.add(paper);
        }
        organization.setTemperature(organization.getTemperature()+1);
        organizationService.save(organization);
        ret.put("papers",papers);
        ret.put("success",true);
        ret.put("total_pages",totalPages);
        ret.put("exec","");
        return ret;
    }

    @PostMapping("/download/paper")
    JSONObject downloadPaper(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        String paperId=params.getString("paper_id");
        Map<String,Object> paper=null;
        try {
            paper=paperService.getPaperByPaperId(paperId);
        }
        catch (Exception e){
            throw new OperationFailed("查询不到这个文献");
        }
        paperService.downloadPaper(paper);
        ret.put("success",true);
        ret.put("exc","下载成功");
        return ret;
    }

    @GetMapping("/home/get_hot_paper")
    JSONObject getHomeHotPapers(){
        JSONObject ret=new JSONObject();
        QueryBuilder QB=paperService.matchAllQB();
        Map<String,SortOrder> sortBy=new HashMap<>();
        sortBy.put("click_time",SortOrder.DESC);
        List<Map<String,Object>> paperMapList=null;
        try{
            paperMapList=paperService.searchQuery(QB,0,sortBy);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        int length=paperMapList.size()>=5?5:paperMapList.size();
        JSONArray hotPapers=new JSONArray();
        for(int i=0;i<length;i++){
            Map<String,Object> paperMap=paperMapList.get(i);
            JSONObject hotPaper=new JSONObject();
            hotPaper.put("title",paperMap.get("title"));
            hotPaper.put("paper_id",paperMap.get("id"));
            hotPaper.put("temperature",paperMap.get("click_time")!=null?paperMap.get("click_time"):0);
            hotPapers.add(hotPaper);
        }
        ret.put("hot_passage",hotPapers);
        ret.put("success",true);
        ret.put("exec","");
        return ret;
    }

    @GetMapping("/door/get_my_papers")
    JSONObject getMyPapers(@RequestHeader("ssid") String ssid,@RequestParam("page") Integer page){
        JSONObject ret=new JSONObject();
        if(ssid==null) throw new OperationFailed("请先登录");
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("请先登录");
        int user_id = (int) session.getAttribute("userId");
        User user=userService.getUserById(user_id);
        String authorId=user.getAuthorId();
        if(authorId==null||authorId.equals("")){
            throw new OperationFailed("请先成为科研成果分享者");
        }
        List<Map<String,Object>> paperMapList=null;
        Integer totalPages=null;
        try {
            QueryBuilder QB=paperService.authorIdQB(authorId);
            paperMapList = paperService.searchQuery(QB,page);
            totalPages = paperService.getTotalPages(QB);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray papers=new JSONArray();
        for (Map<String, Object> paperMap : paperMapList) {
            JSONObject paper = new JSONObject();
            paper.put("download", paperMap.get("download_num")!=null?paperMap.get("download_num"):0);
            paper.put("cited", paperMap.get("n_citation"));
            paper.put("time", paperMap.get("year").toString() + "年");
            paper.put("title", paperMap.get("title"));
            paper.put("id",paperMap.get("id"));
            try {
                List<String> urlList = ((List<String>) paperMap.get("url"));
                paper.put("url", urlList.size() != 0 ? urlList.get(0) : "");
            }
            catch (Exception e){
                paper.put("url",paperMap.get("url")!=null?paperMap.get("url").toString():"");
            }
            papers.add(paper);
        }
        ret.put("total_pages",totalPages);
        ret.put("papers",papers);
        ret.put("success",true);
        ret.put("exec","");
        return ret;
    }

}

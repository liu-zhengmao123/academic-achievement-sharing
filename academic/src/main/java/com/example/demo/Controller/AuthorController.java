package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Service.AuthorService;
import com.example.demo.Utils.OperationFailed;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AuthorController {
    @Autowired
    AuthorService authorService;
    @PostMapping("/author/info")
    JSONObject getAuthorInfo(@RequestBody JSONObject params) {
        JSONObject ret=new JSONObject();
        String authorId=params.getString("author_id");
        Map<String,Object> authorMap=null;
        try {
            authorMap=authorService.getAuthorByAuthorId(authorId);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        authorService.addClickTime(authorMap);
        ret.put("name",authorMap.get("name"));
        ret.put("paper_num",authorService.getPaperNumByAuthor(authorMap));
        ret.put("fields",authorService.getFieldByAuthor(authorMap));
        ret.put("organization",authorService.getOrganizationByAuthor(authorMap));
        ret.put("h_index",authorMap.get("h_index"));
        ret.put("tags",authorMap.get("tags"));
        ret.put("publish",authorService.getPublish(authorId));
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/door/search")
    JSONObject searchDoor(@RequestParam("page") Integer page,@RequestParam("search") String search){
        JSONObject ret=new JSONObject();
        QueryBuilder nameQB=authorService.authorNameQB(search);
        List<Map<String, Object>> authorMapList=null;
        try {
            authorMapList= authorService.searchQuery(nameQB,page);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray authors=new JSONArray();
        for(Map<String,Object> authorMap:authorMapList){
            JSONObject author=new JSONObject();
            author.put("name",authorMap.get("name"));
            author.put("author_id",authorMap.get("id"));
            author.put("papers_count",authorService.getPaperNumByAuthor(authorMap));
            authors.add(author);
        }
        ret.put("authors",authors);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/door/get_door_list")
    JSONObject getDoorList(@RequestParam("page") Integer page){
        JSONObject ret=new JSONObject();
        List<Map<String, Object>> authorMapList=null;
        Integer totalPages=null;
        try {
            authorMapList= authorService.searchQuery(null,page,null,30);
            totalPages = authorService.getTotalPages(null, 30);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray authors=new JSONArray();
        for(Map<String,Object> authorMap:authorMapList){
            JSONObject author=new JSONObject();
            author.put("name",authorMap.get("name"));
            author.put("author_id",authorMap.get("id"));
            author.put("papers_count",authorService.getPaperNumByAuthor(authorMap));
            authors.add(author);
        }
        ret.put("total_pages",totalPages);
        ret.put("authors",authors);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/home/get_hot_author")
    JSONObject getHomeHotAuthor(){
        JSONObject ret=new JSONObject();
        QueryBuilder QB=authorService.matchAllQB();
        Map<String, SortOrder> sortBy=new HashMap<>();
        sortBy.put("click_time",SortOrder.DESC);
        List<Map<String,Object>> authorMapList=null;
        try{
            authorMapList=authorService.searchQuery(QB,0,sortBy);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        int length=authorMapList.size()>=5?5:authorMapList.size();
        JSONArray hotAuthors=new JSONArray();
        for(int i=0;i<length;i++){
            Map<String,Object> authorMap=authorMapList.get(i);
            JSONObject hotAuthor=new JSONObject();
            hotAuthor.put("name",authorMap.get("name"));
            hotAuthor.put("author_id",authorMap.get("id"));
            hotAuthor.put("temperature",authorMap.get("click_time")!=null?authorMap.get("click_time"):0);
            hotAuthors.add(hotAuthor);
        }
        ret.put("hot_author",hotAuthors);
        ret.put("success",true);
        ret.put("exec","");
        return ret;
    }

    @GetMapping("/home/get_hot_words")
    JSONObject getHotWords(){
        JSONObject ret=new JSONObject();
        String str="        [{\n" +
                "          \"key\" : \"Genetics\",\n" +
                "          \"doc_count\" : 9827\n" +
                "        },\n" +
                "        {\n" +
                "          \"key\" : \"Apoptosis\",\n" +
                "          \"doc_count\" : 7885\n" +
                "        },\n" +
                "        {\n" +
                "          \"key\" : \"Gene Expression\",\n" +
                "          \"doc_count\" : 7582\n" +
                "        },\n" +
                "        {\n" +
                "          \"key\" : \"Enzyme\",\n" +
                "          \"doc_count\" : 6278\n" +
                "        },\n" +
                "        {\n" +
                "          \"key\" : \"Risk Factors\",\n" +
                "          \"doc_count\" : 6155\n" +
                "        }]";
        JSONArray hotWords = JSONArray.parseArray(str);
        ret.put("hotwords",hotWords);
        return ret;
    }

}

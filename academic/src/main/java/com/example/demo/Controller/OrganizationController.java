package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.Message;
import com.example.demo.Entity.Organization;
import com.example.demo.Entity.User;
import com.example.demo.Service.*;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.SessionUtil;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class OrganizationController {
    @Autowired
    OrganizationService organizationService;
    @Autowired
    UserService userService;
    @Autowired
    AuthorService authorService;
    @Autowired
    PaperService paperService;
    @Autowired
    MessageService messageService;
    @GetMapping("/list/organization")
    JSONObject getOrgList() {
        JSONObject ret=new JSONObject();
        List<Organization> allOrganization = organizationService.getAllOrganization();
        JSONArray array = new JSONArray();
        for(Organization organization:allOrganization){
            JSONObject info = new JSONObject();
            info.put("id",organization.getId());
            info.put("img",organization.getHead_pic());
            List<User> members = organization.getUserList().stream().filter(user -> !user.isApply()).collect(Collectors.toList());
            info.put("member_num",members.size());
            int num = 0;
            for(User user:members){
                String authorId = user.getAuthorId();
                if(authorId!=null&&!authorId.equals("")) {
                    try{
                        num+=authorService.getPaperNumByAuthorId(authorId);
                    }catch (Exception exception){
                        //do nothing
                    }
                }
            }
            info.put("paper_num",num);
            info.put("name",organization.getOrganizationName());
            info.put("field",organization.getSpecialty());
            array.add(info);
        }
        ret.put("list",array);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @PostMapping("/create/organization")
    JSONObject createAnOrg(@RequestHeader("ssid")String ssid,
                           @RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null||session.getAttribute("userId")==null)throw new OperationFailed("请重新登录");
        User user = userService.getUserById((Integer)session.getAttribute("userId"));
        if(user.getOrganization()!=null)throw new OperationFailed("已经加入组织了");
        if(user.getAuthorId()==null||user.getAuthorId().equals(""))throw new OperationFailed("请先认领门户再申请");
        Organization organization = new Organization();
        organization.setAdmin(user);
        organization.setOrganizationName(params.getString("name"));
        organization.setHead_pic(params.getString("img"));
        organization.setSpecialty(params.getString("field").substring(1,params.getString("field").length()-1));
        user.setOrganization(organization);
        organizationService.save(organization);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @GetMapping("/organization/get_members")
    JSONObject getOrgMembers(@RequestParam("organization_id")int orgId) throws IOException {
        JSONObject ret=new JSONObject();
        Organization organization = organizationService.getOrganizationById(orgId);
        if(organization==null)throw new OperationFailed("机构不存在");
        List<User> userList = organization.getUserList().stream().filter(user -> !user.isApply()).collect(Collectors.toList());
        JSONArray array = new JSONArray();
        for(User user:userList){
            JSONObject info = new JSONObject();
            info.put("id",user.getId());
            info.put("head_pic",user.getHeadPic());
            info.put("name",user.getUserName());
            info.put("description",user.getDescription());
            String specialty = user.getSpecialty();
            if(specialty==null||specialty.equals("")){
                info.put("specialty",new ArrayList<>());
            }
            else {
                String[] specialtys=user.getSpecialty().split(",");
                info.put("specialty",specialtys);
            }
            if(user.getAuthorId()!=null&&!user.getAuthorId().equals("")){
                Map<String,Object> autherInfo=null;
                try {
                    autherInfo=authorService.getAuthorByAuthorId(user.getId().toString());
                }
                catch (Exception exception){}
                if(autherInfo!=null){
                    info.put("cited_count",autherInfo.get("h_index"));
                    info.put("papers_count",autherInfo.get("n_pubs"));
                }
                else {
                    info.put("cited_count",0);
                    info.put("papers_count",0);
                }
            }else{
                info.put("cited_count",0);
                info.put("papers_count",0);
            }
            array.add(info);
        }
        ret.put("members",array);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @GetMapping("/organization/get_apply")
    JSONObject getOrgApply(@RequestParam("organization_id")int orgId){
        JSONObject ret=new JSONObject();
        Organization organization = organizationService.getOrganizationById(orgId);
        if(organization==null)throw new OperationFailed("机构不存在");
        List<User> applyList = organization.getUserList().stream().filter(User::isApply).collect(Collectors.toList());
        JSONArray array = new JSONArray();
        for(User user:applyList){
            JSONObject info = new JSONObject();
            info.put("id",user.getId());
            info.put("head_pic",user.getHeadPic());
            info.put("name",user.getUserName());
            array.add(info);
        }
        ret.put("members",array);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @PostMapping("/join/organization")
    JSONObject applyForAnOrg(@RequestHeader("ssid")String ssid,
                           @RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null||session.getAttribute("userId")==null)throw new OperationFailed("请重新登录");
        User user = userService.getUserById((Integer)session.getAttribute("userId"));
        Organization organization = organizationService.getOrganizationById(params.getInteger("id"));
        if(organization==null)throw new OperationFailed("机构不存在");
        if(!user.isApply()&&user.getOrganization()!=null)throw new OperationFailed("只能加入一个机构");
        if(user.getAuthorId()==null||user.getAuthorId().equals(""))throw new OperationFailed("请认领门户后再申请");
        user.setApply(true);
        user.setOrganization(organization);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @PostMapping("/organization/yes")
    JSONObject judgeApply(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        User user = userService.getUserById(params.getInteger("user_id"));
        int type = params.getInteger("type");
        int organization_id=params.getInteger("organization_id");
        Organization organization=organizationService.getOrganizationById(organization_id);
        if(organization==null) throw new OperationFailed("该机构不存在！");
        Message message=new Message();
        message.setType(3);
        message.setToUser(user);
        message.setTime(new Date());
        if (type == 0) { //拒绝了,则设置org为空
            user.setOrganization(null);
            message.setContent(String.format("您对机构 %s 的申请已被拒绝",organization.getOrganizationName()));
        }else{
            user.setOrganization(organization);
            message.setContent(String.format("您已成功加入机构 %s ",organization.getOrganizationName()));
        }
        user.setApply(false);
        userService.save(user);
        messageService.save(message);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @GetMapping("/organization/papers_count_list")
    JSONObject papersCountList(@RequestParam("organization_id") int organization_id){
        JSONObject ret=new JSONObject();
        Organization organization=organizationService.getOrganizationById(organization_id);
        if(organization==null) throw new OperationFailed("组织不存在");
        List<User> userList = organization.getUserList().stream().filter(user -> !user.isApply()).collect(Collectors.toList());
        JSONArray array = new JSONArray();
        try{
            for (User user : userList) {
                JSONObject info = new JSONObject();
                info.put("username", user.getUserName());
                info.put("user_id", user.getId());
                if (user.getAuthorId() == null||user.getAuthorId().equals("")) {
                    info.put("paper_count", 0);
                } else {
                    info.put("paper_count", authorService.getPaperNumByAuthorId(user.getAuthorId()));
                }
                info.put("head_pic",user.getHeadPic());
                array.add(info);
            }
            array.sort(Comparator.comparing(obj -> ((JSONObject) obj).getInteger("paper_count")).reversed());
        } catch (IOException e) {
            throw new OperationFailed("获取组织论文数排行榜异常");
        }
        int list_length= Math.min(array.size(), 6);
        ret.put("members",array.subList(0,list_length));
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @GetMapping("/organization/get_cited_list")
    JSONObject citedList(@RequestParam("organization") int organization_id){
        JSONObject ret=new JSONObject();
        Organization organization=organizationService.getOrganizationById(organization_id);
        if(organization==null) throw new OperationFailed("组织不存在");
        List<User> userList = organization.getUserList().stream().filter(user -> !user.isApply()).collect(Collectors.toList());
        JSONArray array = new JSONArray();
        try{
            for (User user : userList) {
                JSONObject info = new JSONObject();
                info.put("username", user.getUserName());
                info.put("user_id", user.getId());
                if (user.getAuthorId() == null||user.getAuthorId().equals("")) {
                    info.put("paper_count", 0);
                } else {
                    info.put("paper_count", authorService.getCitedNumByAuthorId(user.getAuthorId()));
                }
                info.put("head_pic",user.getHeadPic());
                array.add(info);
            }
            array.sort(Comparator.comparing(obj -> ((JSONObject) obj).getInteger("paper_count")).reversed());
        } catch (IOException e) {
            throw new OperationFailed("获取组织被引数排行榜异常");
        }
        int list_length= Math.min(array.size(), 6);
        ret.put("members",array.subList(0,list_length));
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @PostMapping("/organization/delete_member")
    JSONObject deleteAMember(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int userId = params.getInteger("member_id");
        User user = userService.getUserById(userId);
        if(user==null)throw new OperationFailed("用户不存在");
        Organization organization = user.getOrganization();
        if(organization!=null&&organization.getAdmin().equals(user)){//管理员退出，解散组织，通知所有成员
            List<User> userList = organization.getUserList();
            for (User i :
                    userList) {
                Message message=new Message();
                message.setTime(new Date());
                message.setType(3);
                message.setToUser(i);
                message.setContent(String.format("您加入(或申请加入)的组织 %s 已解散！",organization.getOrganizationName()));
                messageService.save(message);
                i.setApply(false);
                i.setOrganization(null);
                userService.save(i);
            }
            organizationService.delete(organization);
        }
        user.setOrganization(null);
        user.setApply(false);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }
    @PostMapping("/organization/exit_my_organization")
    JSONObject exitOrgnization(@RequestHeader("ssid") String ssid){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        Organization organization = user.getOrganization();
        if(organization!=null&&organization.getAdmin().equals(user)){//管理员退出，解散组织，通知所有成员
            List<User> userList = organization.getUserList();
            for (User i :
                    userList) {
                Message message=new Message();
                message.setTime(new Date());
                message.setType(3);
                message.setToUser(i);
                message.setContent(String.format("您加入(或申请加入)的组织 %s 已解散！",organization.getOrganizationName()));
                messageService.save(message);
                i.setApply(false);
                i.setOrganization(null);
                userService.save(i);
            }
            organizationService.delete(organization);
        }
        user.setOrganization(null);
        user.setApply(false);
        userService.save(user);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }

    @PostMapping("/organization/edit_name")
    JSONObject editName(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int oid=Integer.parseInt(params.get("organization_id").toString());
        String name=params.get("name").toString();
        Organization organization=organizationService.getOrganizationById(oid);
        if(organization==null) throw new OperationFailed("组织不存在");
        organization.setOrganizationName(name);
        organizationService.save(organization);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }

    @PostMapping("/organization/add_specialty")
    JSONObject addSpecialty(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int oid=Integer.parseInt(params.get("organization_id").toString());
        String specialty=params.get("specialty").toString();
        Organization organization=organizationService.getOrganizationById(oid);
        if(organization==null) throw new OperationFailed("组织不存在");
        //organization.setSpecialty(specialty);
        String originSpec=organization.getSpecialty();
        originSpec+=","+specialty;
        organization.setSpecialty(originSpec);
        organizationService.save(organization);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }

    @PostMapping("/organization/remove_specialty")
    JSONObject removeSpecialty(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int oid=Integer.parseInt(params.get("organization_id").toString());
        String specialty=params.get("specialty").toString();
        Organization organization=organizationService.getOrganizationById(oid);
        if(organization==null) throw new OperationFailed("组织不存在");
        //if(organization.getSpecialty().equals(specialty))organization.setSpecialty("");
        String org_specialty=organization.getSpecialty();
        if(org_specialty==null||org_specialty.equals(""))org_specialty="";
        else{
            List<String> list=new ArrayList<>(Arrays.asList(org_specialty.split(",")));
            list.remove(specialty);
            org_specialty=String.join(",",list);
        }
        organization.setSpecialty(org_specialty);
        organizationService.save(organization);
        ret.put("success",true);
        ret.put("exc","success");
        return ret;
    }

    @PostMapping("/organization/get_info")
    JSONObject getOrganizationInfo(@RequestParam("organization_id")String oid) throws IOException {
        JSONObject ret=new JSONObject();
        int id=Integer.parseInt(oid);
        Organization organization=organizationService.getOrganizationById(id);
        if(organization==null) throw new OperationFailed("组织不存在");
        ret.put("name",organization.getOrganizationName());
        String[] specialtys=organization.getSpecialty().split(",");
        ret.put("specialties",specialtys);
        ret.put("description",organization.getDescription());
        List<User> userList=organization.getUserList();
        int person_count=0,papers_count=0,cited_count=0;
        for(User user:userList){
            person_count++;
            if(user.getAuthorId()!=null&&!user.getAuthorId().equals("")){
                papers_count+=authorService.getCitedNumByAuthorId(user.getAuthorId());
                cited_count+=authorService.getCitedNumByAuthorId(user.getAuthorId());
            }
        }
        ret.put("person_count",Integer.toString(person_count));
        ret.put("papers_count",Integer.toString(papers_count));
        ret.put("cited_count", Integer.toString(cited_count));
        ret.put("head_pic",organization.getHead_pic());
        return ret;
    }

    @PostMapping("/organization/edit_description")
    JSONObject editDescription(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int oid=Integer.parseInt(params.get("organization_id").toString());
        String description=params.get("description").toString();
        Organization organization=organizationService.getOrganizationById(oid);
        if(organization==null) throw new OperationFailed("组织不存在");
        organization.setDescription(description);
        organizationService.save(organization);
        ret.put("success",true);
        return ret;
    }

    @GetMapping("/home/get_hot_orgnization")
    JSONObject getHotOrganization(){
        JSONObject ret=new JSONObject();
        List<Organization> organizationList=organizationService.getHotOrganizations();
        JSONArray hotOrganizations=new JSONArray();
        for (Organization organization:organizationList){
            JSONObject organization_=new JSONObject();
            organization_.put("organization_id",organization.getId());
            organization_.put("name",organization.getOrganizationName());
            organization_.put("temperature",organization.getTemperature());
            hotOrganizations.add(organization_);
        }
        ret.put("hot_organization",hotOrganizations);
        ret.put("success",true);
        ret.put("exec","");
        return ret;
    }
}


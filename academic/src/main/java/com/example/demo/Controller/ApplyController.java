package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.ApplyForPapers;
import com.example.demo.Entity.ApplyForPortal;
import com.example.demo.Entity.User;
import com.example.demo.Service.ApplyService;
import com.example.demo.Service.PaperService;
import com.example.demo.Service.UserService;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.RandomUtil;
import com.example.demo.Utils.SessionUtil;
import com.example.demo.Utils.TimeUtil;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Paper;
import java.util.Date;
import java.util.List;

@RestController
public class ApplyController {
    @Autowired
    UserService userService;
    @Autowired
    ApplyService applyService;
    @Autowired
    PaperService paperService;
    @PostMapping("/obtain_door")
    public JSONObject obtainDoor(@RequestHeader("ssid")String ssid,
                                 @RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int userId = (Integer) SessionUtil.getSession(ssid).getAttribute("userId");
        User user = userService.getUserById(userId);
        if(user==null)throw new OperationFailed("用户不存在");
        if(user.getAuthorId()!=null&&!user.getAuthorId().equals(""))throw new OperationFailed("您已经认领过门户了");
        String authorId = params.getString("author_id");
        if(userService.getUserByAuthorId(authorId)!=null)throw new OperationFailed("该门户已经被认领");
        String img = params.getString("img");
        ApplyForPortal applyForPortal = new ApplyForPortal();
        applyForPortal.setUser(user);
        applyForPortal.setAuthorId(authorId);
        applyForPortal.setImg(img);
        applyForPortal.setApplyTime(new Date());
        applyService.save(applyForPortal);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @GetMapping("/list/obtain")
    public JSONObject getApplyForPortalList(){
        JSONObject ret=new JSONObject();
        List<ApplyForPortal> applyForPortalList = applyService.getAllApplyForPortal();
        JSONArray applyArray = new JSONArray();
        for(ApplyForPortal applyForPortal:applyForPortalList){
            JSONObject info = new JSONObject();
            info.put("id",applyForPortal.getId());
            info.put("email",applyForPortal.getUser().getEmail());
            info.put("time", TimeUtil.dateToStr(applyForPortal.getApplyTime()));
            info.put("pic",applyForPortal.getImg());
            info.put("user_name",applyForPortal.getUser().getUserName());
            applyArray.add(info);
        }
        ret.put("success",true);
        ret.put("exc","");
        ret.put("list",applyArray);
        return ret;
    }

    @PostMapping("/allow/obtain")
    public JSONObject allowObtain(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int applyId = params.getInteger("id");
        ApplyForPortal applyForPortal = applyService.getApplyForPortalById(applyId);
        User user = userService.getUserById(applyForPortal.getUser().getId());
        user.setAuthorId(applyForPortal.getAuthorId());
        userService.save(user);
        applyService.deleteApplyForPortal(applyForPortal);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/reject/obtain")
    public JSONObject rejectObtain(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int applyId = params.getInteger("id");
        applyService.deleteApplyForPortal(applyService.getApplyForPortalById(applyId));
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/door/upload_file")
    JSONObject uploadFile(@RequestBody JSONObject params,
                          @RequestHeader("ssid")String ssid){
        JSONObject ret=new JSONObject();
        ApplyForPapers applyForPapers = new ApplyForPapers();
        applyForPapers.setApplyTime(new Date());
        JSONArray authors = params.getJSONArray("authors");
        JSONArray newAuthors = new JSONArray();
        int userId = (Integer) SessionUtil.getSession(ssid).getAttribute("userId");
        User uploader = userService.getUserById(userId);
        if(uploader==null)throw new OperationFailed("用户不存在");
        applyForPapers.setUserId(uploader.getId());
        if(authors.size()==0)throw new OperationFailed("请添加作者");
        for(int i=0;i<authors.size();i++){
            JSONObject author = authors.getJSONObject(i);
            String email = author.getString("email");
            User user = userService.getUserByEmail(email);
            if(user==null||user.getAuthorId().equals(""))throw new OperationFailed("作者不是学术成果分享者");
            //author.remove("email");
            author.put("id",user.getAuthorId());
            author.put("org",user.getOrganization()==null?"":user.getOrganization().getOrganizationName());
            newAuthors.add(author);
        }
        params.put("pdf",params.getString("url"));
        params.remove("authors");
        params.put("authors",newAuthors);
        String paperId = RandomUtil.generateId();
        params.put("id",paperId);
        applyForPapers.setPaperId(paperId);
        applyForPapers.setJsonObject(params.toJSONString());
        applyService.save(applyForPapers);
        ret.put("success",true);
        ret.put("exec","上传文献成功");
        return ret;
    }

    @GetMapping("/list/upload_paper")
    public JSONObject getApplyForPapers(){
        JSONObject ret=new JSONObject();
        List<ApplyForPapers> apply = applyService.getAllApplyForPapers();
        JSONArray list = new JSONArray();
        for(ApplyForPapers applyForPapers:apply){
            JSONObject paper = JSONObject.parseObject(applyForPapers.getJsonObject()) ;
            paper.put("user_id",applyForPapers.getUserId());
            //paper.put("paper_id",applyForPapers.getPaperId());
            list.add(paper);
        }
        ret.put("success",true);
        ret.put("exc","");
        ret.put("list",list);
        return ret;
    }

    @PostMapping("/allow/upload_paper")
    public JSONObject allowPaper(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        String paperId = params.getString("paper_id");
        ApplyForPapers applyForPapers = applyService.getApplyForPapersByPaperId(paperId);
        if(applyForPapers==null)throw new OperationFailed("申请不存在");
        paperService.pushPaper(JSONObject.parseObject(applyForPapers.getJsonObject()));
        applyService.delete(applyForPapers);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }

    @PostMapping("/refuse/upload_paper")
    public JSONObject refusePaper(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        String paperId = params.getString("paper_id");
        ApplyForPapers applyForPapers = applyService.getApplyForPapersByPaperId(paperId);
        if(applyForPapers==null)throw new OperationFailed("申请不存在");
        applyService.delete(applyForPapers);
        ret.put("success",true);
        ret.put("exc","");
        return ret;
    }
}

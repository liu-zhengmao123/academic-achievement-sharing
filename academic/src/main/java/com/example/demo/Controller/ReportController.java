package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.ReportInfo;
import com.example.demo.Entity.User;
import com.example.demo.Service.ReportService;
import com.example.demo.Service.UserService;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import javax.jws.soap.SOAPBinding;
import java.util.Date;
import java.util.List;

@RestController
public class ReportController {
    @Autowired
    ReportService reportService;
    @Autowired
    UserService userService;
    @PostMapping("/report")//举报接口
    public JSONObject report(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        Integer userId=params.getInteger("author_id");
        String reason=params.getString("report_reason");
        ReportInfo reportInfo=new ReportInfo();
        reportInfo.setReason(reason);
        reportInfo.setUserId(userId);
        reportInfo.setTime(new Date());
        reportService.saveReport(reportInfo);
        ret.put("success",true);
        ret.put("exc","举报成功，等待管理员处理");
        return ret;
    }

    @PostMapping("/report/ignore")
    public JSONObject ignoreReport(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        try{
            reportService.deleteReport(params.getInteger("report_id"));
        }
        catch (Exception e){
            throw new OperationFailed("举报ID不存在");
        }
        ret.put("success",true);
        ret.put("exc","已忽略该举报信息");
        return ret;
    }

    @PostMapping("/list/report")
    public JSONObject getAllReport(){
        JSONObject ret=new JSONObject();
        JSONArray list=new JSONArray();
        List<ReportInfo> reportInfos=reportService.getAllReports();
        for(ReportInfo reportInfo:reportInfos){
            JSONObject report=new JSONObject();
            Integer userId=reportInfo.getUserId();
            User user=userService.getUserById(userId);
            report.put("report_reason",reportInfo.getReason());
            report.put("time", TimeUtil.dateToCal(reportInfo.getTime()));
            report.put("id",reportInfo.getId());
            report.put("user_id",reportInfo.getUserId());
            report.put("user_name",user.getUserName());
            report.put("email",user.getEmail());
            report.put("organization",user.getOrganization()!=null?user.getOrganization().getOrganizationName():"");
            list.add(report);
        }
        ret.put("list",list);
        ret.put("success",true);
        ret.put("exc","成功获取所有举报信息");
        return ret;
    }

}

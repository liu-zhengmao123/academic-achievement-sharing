package com.example.demo.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Entity.Message;
import com.example.demo.Entity.Organization;
import com.example.demo.Entity.User;
import com.example.demo.Service.MessageService;
import com.example.demo.Service.OrganizationService;
import com.example.demo.Service.UserService;
import com.example.demo.Utils.OperationFailed;
import com.example.demo.Utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MessageController {
    @Autowired
    MessageService messageService;
    @Autowired
    OrganizationService organizationService;
    @Autowired
    UserService userService;

    @PostMapping("/notice/send_message")
    JSONObject sendMessage(@RequestBody JSONObject params){
        JSONObject ret=new JSONObject();
        int organization_id=Integer.parseInt(params.get("organization_id").toString());
        String content=params.get("message").toString();
        Organization organization=organizationService.getOrganizationById(organization_id);
        if(organization==null) throw new OperationFailed("组织不存在");
        List<User> user_list=organization.getUserList().stream().filter(user -> !user.isApply()).collect(Collectors.toList());
        for (User user :
                user_list) {
            try{
                Message message = new Message();
                message.setFromOrganization(organization);
                message.setType(2);
                message.setContent(content);
                message.setToUser(user);
                message.setTime(new Date());
                messageService.save(message);
            }catch (Exception e){
                throw new OperationFailed("组织发送消息异常");
            }
        }

        ret.put("success",true);
        return ret;
    }

    @GetMapping("/notice/get_notice")
    JSONObject getMessage(@RequestHeader("ssid")String ssid){
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int user_id = (int) session.getAttribute("userId");
        User user = userService.getUserById(user_id);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            List<Message> messages = messageService.getMessageByToUser(user);
            JSONArray contents=new JSONArray();
            for (Message i :
                    messages) {
                JSONObject content=new JSONObject();
                content.put("id",i.getId());
                content.put("content",i.getContent());
                content.put("has_read",i.isHasRead());
                content.put("time",sdf.format(i.getTime()));
                content.put("source",i.getType());
                contents.add(content);
            }
            ret.put("contents",contents);
            ret.put("success",true);
        }catch (Exception e){
            e.printStackTrace();
            throw new OperationFailed("获取消息异常");
        }

        return ret;
    }

    @PostMapping("/notice/read")
    JSONObject readMessage(@RequestHeader("ssid")String ssid,
                           @RequestBody JSONObject params) {
        JSONObject ret=new JSONObject();
        HttpSession session = SessionUtil.getSession(ssid);
        if(session==null) throw new OperationFailed("用户未登录");
        int message_id = params.getInteger("id");
        Message message=messageService.getMessageById(message_id);
        message.setHasRead(true);
        messageService.save(message);
        ret.put("success",true);
        return ret;
    }

    @PostMapping("/notice/delete")
    JSONObject deleteMessage(@RequestBody JSONObject params) {
        JSONObject ret=new JSONObject();
        int message_id = params.getInteger("id");
        Message message=messageService.getMessageById(message_id);
        if(message!=null) messageService.delete(message);
        ret.put("success",true);
        return ret;
    }

}

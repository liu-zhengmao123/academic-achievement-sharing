package com.example.demo.Repository;

import com.alibaba.fastjson.JSONObject;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PaperDao{
    @Resource
    RestHighLevelClient client;
    final Integer pageSize=10;
    final String paperDb="origin_papers";
    public Map<String,Object> getPaperByPaperId(String paperId) throws IOException{
        QueryBuilder QB=QueryBuilders.termQuery("id",paperId);
        List<Map<String,Object>> paperList=searchQuery(QB,0);
        return paperList.size()!=0?paperList.get(0):null;
    }

    public List<Map<String,Object>> getPaperByAuthorId(String authorId) throws IOException{
        QueryBuilder QB=QueryBuilders.termQuery("authors.id",authorId);
        return searchQuery(QB,0);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page, Map<String,SortOrder> sortBy) throws IOException{
        List<Map<String,Object>> ret=new ArrayList<>();
        SearchRequest request=new SearchRequest();
        request.indices(paperDb);
        SearchSourceBuilder searchSourceBuilder=null;
        if(page>=0) {
            searchSourceBuilder = new SearchSourceBuilder().query(QB).from(page).size(pageSize);
        }
        else {
            searchSourceBuilder = new SearchSourceBuilder().query(QB).size(10000);
        }
        if(sortBy!=null) {
            for (String key : sortBy.keySet()) {
                searchSourceBuilder.sort(key, sortBy.get(key));
            }
        }
        request.source(searchSourceBuilder);
        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
        SearchHits searchHits=response.getHits();
        for (SearchHit hit:searchHits){
            Map<String, Object> source = hit.getSourceAsMap();
            source.put("es_id",hit.getId());
            ret.add(hit.getSourceAsMap());
        }
        return ret;
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page) throws IOException {
        return searchQuery(QB,page,null);
//        List<Map<String,Object>> ret=new ArrayList<>();
//        SearchRequest request=new SearchRequest();
//        request.indices(paperDb);
//        request.source(new SearchSourceBuilder().query(QB).from(page).size(10));
//        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
//        SearchHits searchHits=response.getHits();
//        for (SearchHit hit:searchHits){
//            Map<String, Object> source = hit.getSourceAsMap();
//            source.put("es_id",hit.getId());
//            ret.add(hit.getSourceAsMap());
//        }
//        return ret;
    }

    public int getTotalPages(QueryBuilder QB) throws IOException {
        SearchRequest request=new SearchRequest();
        request.indices(paperDb);
        request.source(new SearchSourceBuilder().query(QB));
        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
        return (int)Math.ceil(response.getHits().getTotalHits().value*1.0/pageSize);
    }

    public void addClickTime(Map<String,Object> paper){
        if(paper.get("click_time")!=null){
            paper.put("click_time",(Integer)paper.get("click_time")+1);
        }
        else paper.put("click_time",1);
        update(paper);
    }

    public void addDownLoadNum(Map<String,Object> paper){
        if(paper.get("download_num")!=null){
            paper.put("download_num",(Integer)paper.get("download_num")+1);
        }
        else paper.put("download_num",1);
        update(paper);
    }

    public void update(Map<String,Object> paper){
        IndexRequest request=new IndexRequest(paperDb);
        if(paper.get("es_id")!=null){
            request.id(paper.get("es_id").toString());
            paper.remove("es_id");
        }
        request.source(paper);
        update(request);
    }

    public void update(IndexRequest indexRequest){
        ActionListener<IndexResponse> listener = new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
            }
            @Override
            public void onFailure(Exception e) {
            }
        };
        client.indexAsync(indexRequest,RequestOptions.DEFAULT,listener);
    }
}

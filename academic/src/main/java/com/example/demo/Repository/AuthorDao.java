package com.example.demo.Repository;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AuthorDao {
    @Resource
    RestHighLevelClient client;
    final String authorDb="origin_authors";
    final Integer pageSize=10;
    public Map<String,Object> getAuthorByAuthorId(String authorId) throws IOException{
        QueryBuilder QB=QueryBuilders.termQuery("id",authorId);
        List<Map<String,Object>> authorList=searchQuery(QB,0);
        return authorList.size()!=0?authorList.get(0):null;
    }

    @SuppressWarnings("all")
    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page, Map<String, SortOrder> sortBy) throws IOException{
        List<Map<String,Object>> ret=new ArrayList<>();
        SearchRequest request=new SearchRequest();
        request.indices(authorDb);
        SearchSourceBuilder searchSourceBuilder=new SearchSourceBuilder().query(QB).from(page).size(pageSize);
        if(sortBy!=null) {
            for (String key : sortBy.keySet()) {
                searchSourceBuilder.sort(key, sortBy.get(key));
            }
        }
        request.source(searchSourceBuilder);
        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
        SearchHits searchHits=response.getHits();
        for (SearchHit hit:searchHits){
            Map<String, Object> source = hit.getSourceAsMap();
            source.put("es_id",hit.getId());
            ret.add(hit.getSourceAsMap());
        }
        return ret;
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page) throws IOException {
        return searchQuery(QB, page, null);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page, Map<String, SortOrder> sortBy, int page_size) throws IOException{
        List<Map<String,Object>> ret=new ArrayList<>();
        SearchRequest request=new SearchRequest();
        request.indices(authorDb);
        SearchSourceBuilder searchSourceBuilder=new SearchSourceBuilder().query(QB).from(page).size(page_size);
        if(sortBy!=null) {
            for (String key : sortBy.keySet()) {
                searchSourceBuilder.sort(key, sortBy.get(key));
            }
        }
        request.source(searchSourceBuilder);
        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
        SearchHits searchHits=response.getHits();
        for (SearchHit hit:searchHits){
            Map<String, Object> source = hit.getSourceAsMap();
            source.put("es_id",hit.getId());
            ret.add(hit.getSourceAsMap());
        }
        return ret;
    }

    public void addClickTime(Map<String,Object> author){
        if(author.get("click_time")!=null){
            author.put("click_time",(Integer)author.get("click_time")+1);
        }
        else author.put("click_time",1);
        update(author);
    }

    public void update(Map<String,Object> paper){
        IndexRequest request=new IndexRequest(authorDb);
        if(paper.get("es_id")!=null){
            request.id(paper.get("es_id").toString());
            paper.remove("es_id");
        }
        request.source(paper);
        update(request);
    }

    public void update(IndexRequest indexRequest){
        ActionListener<IndexResponse> listener = new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
            }
            @Override
            public void onFailure(Exception e) {
            }
        };
        client.indexAsync(indexRequest,RequestOptions.DEFAULT,listener);
    }

    public int getTotalPages(QueryBuilder QB, Integer page_size) throws IOException {
        SearchRequest request=new SearchRequest();
        request.indices(authorDb);
        request.source(new SearchSourceBuilder().query(QB));
        SearchResponse response=client.search(request, RequestOptions.DEFAULT);
        return (int)Math.ceil(response.getHits().getTotalHits().value*1.0/page_size);
    }
}

package com.example.demo.Repository;

import com.example.demo.Entity.ReportInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportDao extends JpaRepository<ReportInfo,Integer> {
}

package com.example.demo.Repository;

import com.example.demo.Entity.Folder;
import com.example.demo.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FolderDao extends JpaRepository<Folder,Integer> {
    List<Folder> getFoldersByUser(User user);
    Folder getFolderById(int folderId);
}

package com.example.demo.Repository;

import com.example.demo.Entity.History;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoryDao extends JpaRepository<History,Integer> {
    History findHistoryById(int id);
}

package com.example.demo.Repository;

import com.example.demo.Entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentDao extends JpaRepository<Comment,Integer> {
    List<Comment> getAllByPaperId(String paperId);
    Comment findCommentById(int commentId);
}

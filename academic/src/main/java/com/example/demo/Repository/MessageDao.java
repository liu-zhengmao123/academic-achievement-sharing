package com.example.demo.Repository;

import com.example.demo.Entity.Message;
import com.example.demo.Entity.Organization;
import com.example.demo.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageDao extends JpaRepository<Message,Integer> {
    Message findMessageById(int messageId);
    List<Message> getMessagesByFromUser(User from_user);
    List<Message> getMessagesByFromOrganization(Organization organization);
    List<Message> getMessagesByToUser(User to_user);
    void deleteMessageById(int messageId);
}

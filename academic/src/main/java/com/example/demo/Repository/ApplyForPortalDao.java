package com.example.demo.Repository;

import com.example.demo.Entity.ApplyForPortal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplyForPortalDao extends JpaRepository<ApplyForPortal,Integer> {
    ApplyForPortal findApplyForPortalById(int id);
}

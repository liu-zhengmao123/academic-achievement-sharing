package com.example.demo.Repository;

import com.example.demo.Entity.Organization;
import org.aspectj.weaver.ast.Or;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrganizationDao extends JpaRepository<Organization,Integer> {
    Organization findOrganizationById(int id);
    List<Organization> getAllByOrderByTemperature(Pageable pageable);
}

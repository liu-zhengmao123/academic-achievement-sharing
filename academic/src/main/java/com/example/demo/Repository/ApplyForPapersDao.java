package com.example.demo.Repository;

import com.example.demo.Entity.ApplyForPapers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplyForPapersDao extends JpaRepository<ApplyForPapers,Integer> {
    ApplyForPapers findByPaperId(String paperId);
}

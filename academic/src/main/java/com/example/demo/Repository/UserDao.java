package com.example.demo.Repository;

import com.example.demo.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDao extends JpaRepository<User, Integer> {
    List<User> getUsersByIsBan(boolean isBan);
    User findUserByEmail(String email);
    User findUserById(int id);
    User findUserByAuthorId(String authorId);
}

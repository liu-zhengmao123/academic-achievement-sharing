package com.example.demo.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.Repository.AuthorDao;
import com.example.demo.Utils.OperationFailed;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuthorService {
    @Autowired
    AuthorDao authorDao;
    @Autowired
    PaperService paperService;
    public Map<String,Object> getAuthorByAuthorId(String authorId) throws IOException{
        return authorDao.getAuthorByAuthorId(authorId);
    }

    public QueryBuilder authorNameQB(String authorName){
        return QueryBuilders.matchQuery("name",authorName);
    }

    public QueryBuilder matchAllQB(){
        return QueryBuilders.matchAllQuery();
    }

    public Integer getPaperNumByAuthorId(String authorId) throws IOException{
        Map<String,Object> author=getAuthorByAuthorId(authorId);
        return getPaperNumByAuthor(author);
    }

    public Integer getPaperNumByAuthor(Map<String,Object> author){
        if(author==null) return 0;
        List<Map<String,Object>> tags= (List<Map<String, Object>>) author.get("tags");
        Integer count=0;
        for(Map<String,Object> tag:tags){
            count += (Integer)tag.get("w");
        }
        return count;
    }

    public String getFieldByAuthor(Map<String,Object> author){
        List<Map<String,Object>> tags= (List<Map<String, Object>>) author.get("tags");
        StringBuilder sb=new StringBuilder();
        for(Map<String,Object> tag:tags){
            sb.append(tag.get("t").toString());
            sb.append(",");
        }
        return sb.substring(0,sb.length()-1);
    }

    public int getCitedNumByAuthorId(String authorId) throws IOException {
        List<Map<String, Object>> mapList = paperService.getPaperByAuthorId(authorId);
        int cited=0;
        for (Map<String, Object> o :
                mapList) {
            if(o.containsKey("n_citation")){
                cited+=((Integer)o.get("n_citation"));
            }
        }
        return cited;
    }

    @SuppressWarnings("all")
    public String getOrganizationByAuthor(Map<String,Object> author){
        List<String> orgs= ((List<String>) author.get("orgs"));
        return orgs.get(0);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page) throws IOException {
        return authorDao.searchQuery(QB, page);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page, Map<String, SortOrder> sortBy) throws IOException{
        return authorDao.searchQuery(QB, page, sortBy);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page, Map<String, SortOrder> sortBy,int page_size) throws IOException{
        return authorDao.searchQuery(QB, page, sortBy,page_size);
    }

    public void addClickTime(Map<String,Object> authorMap){
        authorDao.addClickTime(authorMap);
    }

    public JSONArray getPublish(String authorId){
        List<Map<String, Object>> paperMapList = null;
        try {
            paperMapList = paperService.getPaperByAuthorId(authorId);
        }
        catch (Exception e){
            throw new OperationFailed("数据库查询异常");
        }
        JSONArray publishs=new JSONArray();
        Map<Integer,Integer> year_n=new HashMap<>();
        for(Map<String,Object> paperMap:paperMapList){
            if (paperMap.get("year")==null) continue;
            Integer year= ((Integer) paperMap.get("year"));
            if (year_n.get(year) == null) {
                year_n.put(year, 1);
            } else {
                year_n.put(year, year_n.get(year) + 1);
            }
        }
        for(Integer year:year_n.keySet()){
            JSONObject publish=new JSONObject();
            publish.put("year",year);
            publish.put("n",year_n.get(year));
            publishs.add(publish);
        }
        return publishs;
    }

    public Integer getTotalPages(QueryBuilder QB, Integer page_size) throws IOException {
        return authorDao.getTotalPages(QB, page_size);
    }
}

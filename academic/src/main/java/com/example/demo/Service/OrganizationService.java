package com.example.demo.Service;

import com.example.demo.Entity.Organization;
import com.example.demo.Repository.OrganizationDao;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationService {
    @Autowired
    OrganizationDao organizationDao;
    public List<Organization> getAllOrganization(){
        return organizationDao.findAll();
    }
    public void save(Organization organization){
        organizationDao.save(organization);
    }
    public Organization getOrganizationById(int id){
        return organizationDao.findOrganizationById(id);
    }
    public List<Organization> getHotOrganizations(){
        Sort sort=Sort.by(Sort.Order.asc("id"));
        Pageable pageable= PageRequest.of(0,5,sort);
        return organizationDao.getAllByOrderByTemperature(pageable);
    }
    public void delete(Organization organization) {organizationDao.delete(organization);}

}

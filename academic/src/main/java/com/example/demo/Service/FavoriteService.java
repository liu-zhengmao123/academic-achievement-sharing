package com.example.demo.Service;

import com.example.demo.Entity.Favorite;
import com.example.demo.Repository.FavoriteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FavoriteService {
    @Autowired
    FavoriteDao favoriteDao;
    public void save(Favorite favorite){
        favoriteDao.save(favorite);
    }
}

package com.example.demo.Service;

import com.example.demo.Entity.ApplyForPapers;
import com.example.demo.Entity.ApplyForPortal;
import com.example.demo.Repository.ApplyForPapersDao;
import com.example.demo.Repository.ApplyForPortalDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// 暂定所有Apply都用这个Service
@Service
public class ApplyService {
    @Autowired
    ApplyForPortalDao applyForPortalDao;
    @Autowired
    ApplyForPapersDao applyForPapersDao;
    public void save(ApplyForPortal applyForPortal){
        applyForPortalDao.save(applyForPortal);
    }

    public List<ApplyForPortal> getAllApplyForPortal(){
        return applyForPortalDao.findAll();
    }

    public ApplyForPortal getApplyForPortalById(int id){
        return applyForPortalDao.findApplyForPortalById(id);
    }

    public void deleteApplyForPortal(ApplyForPortal applyForPortal){
        applyForPortalDao.delete(applyForPortal);
    }

    public void save(ApplyForPapers applyForPapers){
        applyForPapersDao.save(applyForPapers);
    }

    public List<ApplyForPapers> getAllApplyForPapers(){
        return applyForPapersDao.findAll();
    }

    public ApplyForPapers getApplyForPapersByPaperId(String paperId){
        return applyForPapersDao.findByPaperId(paperId);
    }

    public void delete(ApplyForPapers applyForPapers){
        applyForPapersDao.delete(applyForPapers);
    }

}

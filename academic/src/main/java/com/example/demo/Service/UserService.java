package com.example.demo.Service;

import com.example.demo.Entity.History;
import com.example.demo.Entity.User;
import com.example.demo.Repository.HistoryDao;
import com.example.demo.Repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {
    @Autowired
    UserDao userDao;
    @Autowired
    HistoryDao historyDao;
    public List<User> getAllByBan(boolean isBan){
        return userDao.getUsersByIsBan(isBan);
    }
    public User getUserById(int userId){
        return userDao.findUserById(userId);
    }
    public void save(User user){
        userDao.save(user);
    }
    public User getUserByEmail(String email){
        return userDao.findUserByEmail(email);
    }
    public void deleteHistoryById(int historyId) {historyDao.deleteById(historyId);}
    public History findHistoryById(int historyId) {return historyDao.findHistoryById(historyId);}
    public List<User> getAllUsers(){
        return userDao.findAll();
    }
    public User getUserByAuthorId(String authorId){
        return userDao.findUserByAuthorId(authorId);
    }
}

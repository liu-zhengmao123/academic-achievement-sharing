package com.example.demo.Service;

import com.example.demo.Entity.Comment;
import com.example.demo.Repository.CommentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    @Autowired
    CommentDao commentDao;
    public List<Comment> getCommentByPaperId(String paperId){
        return commentDao.getAllByPaperId(paperId);
    }
    public void save(Comment comment){
        commentDao.save(comment);
    }
    public Comment getCommentById(int commentId){return commentDao.findCommentById(commentId);}
    public void delete(Comment comment){
        commentDao.delete(comment);
    }
}

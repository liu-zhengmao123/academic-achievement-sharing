package com.example.demo.Service;

import com.example.demo.Entity.Favorite;
import com.example.demo.Entity.Folder;
import com.example.demo.Entity.User;
import com.example.demo.Repository.FolderDao;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FolderService {
    @Autowired
    FolderDao folderDao;
    public List<Folder> getFoldersByUser(User user){
        return folderDao.getFoldersByUser(user);
    }
    public void save(Folder folder){folderDao.save(folder);}
    public Folder getById(int folderId){
        return folderDao.getFolderById(folderId);
    }
    public boolean isStored(User user, String paperId){
        List<Folder> folderList = user.getFolderList();
        for(Folder folder:folderList){
            List<Favorite> favoriteList = folder.getPaperList();
            for(Favorite favorite:favoriteList){
                if(favorite.getPaperId().equals(paperId)) return true;
            }
        }
        return false;
    }
    public void delete(Folder folder){
        folderDao.delete(folder);
    }
}

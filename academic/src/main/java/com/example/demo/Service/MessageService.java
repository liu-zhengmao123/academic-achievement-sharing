package com.example.demo.Service;

import com.example.demo.Entity.Message;
import com.example.demo.Entity.Organization;
import com.example.demo.Entity.User;
import com.example.demo.Repository.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    MessageDao messageDao;
    public List<Message> getMessageByFromUser(User user){
        return messageDao.getMessagesByFromUser(user);
    }
    public List<Message> getMessageByToUser(User user){
        return messageDao.getMessagesByToUser(user);
    }
    public List<Message> getMessageByFromOrganization(Organization organization){
        return messageDao.getMessagesByFromOrganization(organization);
    }
    public void save(Message message){
        messageDao.save(message);
    }
    public Message getMessageById(int messageId){return messageDao.findMessageById(messageId);}
    public void delete(Message message){ messageDao.delete(message);}
}

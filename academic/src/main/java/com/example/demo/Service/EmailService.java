package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender mailSender;
    public void sendEmail(String to, String subject, String content, File file) throws MessagingException {
        MimeMessage mineMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mineMessage,true);
        String from = "se_email_sender@163.com";
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(content);
        if(file != null) {
            FileSystemResource attachedFile = new FileSystemResource(file);
            helper.addAttachment(file.getName(),attachedFile);
        }
        mailSender.send(mineMessage);
    }

    public void sendEmail(String to, String subject, String content) throws MessagingException {
        sendEmail(to,subject,content,null);
    }
}

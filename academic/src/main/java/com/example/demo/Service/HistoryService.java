package com.example.demo.Service;

import com.example.demo.Entity.History;
import com.example.demo.Entity.User;
import com.example.demo.Repository.HistoryDao;
import com.example.demo.Repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class HistoryService {
    @Autowired
    HistoryDao historyDao;
    @Autowired
    UserDao userDao;
    public void save(User user, Map<String,Object> paper){
        History history=new History();
        history.setPaperId(paper.get("id").toString());
        history.setPaperTitle(paper.get("title").toString());
        history.setUser(user);
        history.setTime(new Date());
        historyDao.save(history);
    }
}

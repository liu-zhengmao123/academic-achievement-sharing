package com.example.demo.Service;

import com.example.demo.Repository.PaperDao;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class PaperService {
    @Autowired
    PaperDao paperDao;
    //将所有作者拼接成x1,x2,x3的形式
    public String getAllAuthorNamesOfPaper(Map<String,Object> paperMap){
        StringBuilder sb=new StringBuilder();
        List<Map<String,Object>> authors= (List<Map<String, Object>>) paperMap.get("authors");
        for(Map<String,Object> author:authors){
            try {
                sb.append(author.get("name").toString());
                sb.append(";");
            }
            catch (Exception e) {
                sb.append(";");
            }
        }
        if(sb.length()!=0){
            return sb.substring(0,sb.length()-1);
        }
        return sb.toString();
    }

    public String getAllAuthorIdsOfPaper(Map<String,Object> paperMap){
        StringBuilder sb=new StringBuilder();
        List<Map<String,Object>> authors= (List<Map<String, Object>>) paperMap.get("authors");
        for(Map<String,Object> author:authors){
            try {
                sb.append(author.get("id").toString());
                sb.append(",");
            }
            catch (Exception e) {
                sb.append(",");
            }
        }
        if(sb.length()!=0){
            return sb.substring(0,sb.length()-1);
        }
        return sb.toString();
    }

    public Map<String,Object> getPaperByPaperId(String paperId) throws IOException {
        return paperDao.getPaperByPaperId(paperId);
    }

    public List<Map<String,Object>> getPaperByAuthorId(String authorId) throws IOException {
        return paperDao.getPaperByAuthorId(authorId);
    }

    public QueryBuilder matchAllQB(){
        return QueryBuilders.matchAllQuery();
    }

    public QueryBuilder yearQB(Integer startYear, Integer endYear){
        return QueryBuilders.rangeQuery("year").from(startYear).to(endYear);
    }

    public QueryBuilder typeQB(String type){
        String body="{\n" +
                "    \"match_phrase\": {\n" +
                "      \"type\": \"";
        body+=type;
        body+="\"   }\n" +
                "  }";
        return QueryBuilders.wrapperQuery(body);
    }

    public QueryBuilder authorIdQB(String authorId){
        String body="{\n" +
                "    \"match_phrase\": {\n" +
                "      \"authors.id\": \"";
        body+=authorId;
        body+="\"   }\n" +
                "  }";
        return QueryBuilders.wrapperQuery(body);
    }

    public QueryBuilder authorNameQB(String authorName){
        String body="{\n" +
                "    \"match_phrase\": {\n" +
                "      \"authors.name\": \"";
        body+=authorName;
        body+="\"   }\n" +
                "  }";
        return QueryBuilders.wrapperQuery(body);
    }

    public QueryBuilder abstractQB(String abstractKeyWord){
        return QueryBuilders.matchQuery("abstract",abstractKeyWord);
    }

    public QueryBuilder titleQB(String titleKeyWord){
        return QueryBuilders.matchQuery("title", titleKeyWord);
    }

    public QueryBuilder isbnQB(String ISBN){
        return QueryBuilders.termQuery("isbn",ISBN);
    }

    public QueryBuilder issnQB(String ISSN){
        return QueryBuilders.termQuery("issn",ISSN);
    }

    public QueryBuilder doiQB(String DOI){
        return QueryBuilders.termQuery("doi",DOI);
    }

    public QueryBuilder keyWordQB(String keyword){
        return QueryBuilders.matchQuery("keywords",keyword);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page) throws IOException {
        return paperDao.searchQuery(QB, page);
    }

    public List<Map<String,Object>> searchQuery(QueryBuilder QB, int page, Map<String, SortOrder> sortBy) throws IOException{
        return paperDao.searchQuery(QB, page, sortBy);
    }

    public int getTotalPages(QueryBuilder QB) throws IOException {
        return paperDao.getTotalPages(QB);
    }

    public void addClickTime(Map<String,Object> paper){
        paperDao.addClickTime(paper);
    }

    public void pushPaper(Map<String,Object> paper){
        paperDao.update(paper);
    }

    public void changeAuthorOfPaper(String oldAuthorId,String newAuthorId,List<Map<String,Object>> paperList){
        for(Map<String,Object> paper:paperList){
            List<Map<String,Object>> authorList= ((List<Map<String, Object>>) paper.get("authors"));
            for(Map<String,Object> author:authorList){
                if(author.get("id").toString().equals(oldAuthorId)){
                    author.put("id",newAuthorId);
                    break;
                }
            }
            paperDao.update(paper);
        }
    }

    public void downloadPaper(Map<String,Object> paper){
        paperDao.addDownLoadNum(paper);
    }

    public String exportCitation(Map<String,Object> paper){
        if(paper.get("type")==null){
            return "";
        }
        StringBuilder sb=new StringBuilder();
        try {
            String issue = paper.get("issue").toString();
            String authors = getAllAuthorNamesOfPaper(paper);
            String title = paper.get("title").toString();
            String venue = "";
            Map<String, Object> venue_ = (Map<String, Object>) paper.get("venue");
            try {
                venue = venue_.get("raw").toString();
            } catch (Exception e) {
                return "";
            }
            Integer year = ((Integer) paper.get("year"));
            if (paper.get("type").equals("book")) {
                sb.append(authors);
                sb.append(". ");
                sb.append(title);
                sb.append("[J]. ");
                sb.append(venue);
                sb.append(", ");
                sb.append(String.valueOf(year));
                sb.append("(" + issue + ").");
            } else {
                sb.append(authors);
                sb.append(". ");
                sb.append(title);
                sb.append("[M]. ");
                sb.append(issue);
                sb.append(". ");
                sb.append(venue);
                sb.append(", ");
                sb.append(String.valueOf(year));
                sb.append(".");
            }
        }
        catch (Exception e){
            return "";
        }
        try {
            String pageStart = paper.get("page_start").toString();
            String pageEnd = paper.get("page_end").toString();
            String page = pageStart + "+" + pageEnd;
            return sb.substring(0, sb.length() - 1) + ":" + page + ".";
        }
        catch (Exception e){
            return sb.toString();
        }
    }
}

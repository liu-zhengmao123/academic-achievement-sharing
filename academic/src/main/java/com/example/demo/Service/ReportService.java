package com.example.demo.Service;

import com.example.demo.Entity.ReportInfo;
import com.example.demo.Repository.ReportDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {
    @Autowired
    ReportDao reportDao;
    public void saveReport(ReportInfo reportInfo){
        reportDao.save(reportInfo);
    }

    public void deleteReport(Integer reportId) {
        reportDao.deleteById(reportId);
    }

    public List<ReportInfo> getAllReports(){
        return reportDao.findAll();
    }
}
